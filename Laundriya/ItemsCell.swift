//
//  ItemsCell.swift
//  Washer-Client
//
//  Created by Amr on 2/7/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import UIKit
protocol ItemsCellDelagete {
    func ItemsCellSelected(_ cell: ItemsCell)
}
class ItemsCell: UITableViewCell {
    
    @IBOutlet weak var OrderItems: UILabel!
    @IBOutlet weak var OrderDates: UILabel!
    @IBOutlet weak var OrderPrice: UILabel!
    @IBOutlet weak var OrderStatus: UILabel!
    var order: Order?
    var delegate : ItemsCellDelagete?
    override func awakeFromNib() {
        super.awakeFromNib()
//        orderID.layer.cornerRadius = 8
//        orderidd.layer.cornerRadius = 8
//        orderStatus.layer.cornerRadius = 8
    }

    @IBAction func setselect()
    {
        self.delegate?.ItemsCellSelected(self)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
