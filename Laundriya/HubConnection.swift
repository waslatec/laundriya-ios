////
////  TestHubCon.swift
////  Washer-Client
////
////  Created by Amr on 2/6/17.
////  Copyright © 2017 WaslTec. All rights reserved.
////
//
import Foundation
import SwiftR
import CCMPopup

class HubConnection: NSObject {
    
static var washHub: Hub!
static var connection: SignalR!
    
   static func startConnection()   {
    
connection = SignalR("http://washingservice.wasltec.com")
//connection.useWKWebView = true
connection.signalRVersion = .v2_2_1

//chvarub = Hub("washHub")
        
     washHub = connection.createHubProxy("washHub")
    
washHub.on("hi") { args in
    print("-----------------------------------------------")
    print(args)
}
washHub.on("PendingOrder") { args in
        print("-----------------------------------------------PendingOrder")
        print(args)
    let orderId = args?[0]
//    NotificationCenter.default.post(name: Notification.Name("PendingOrder"), object: nil)
    
    let orderIdd:[String: Int] = ["orderId": orderId as! Int]
    NotificationCenter.default.post(name: Notification.Name("PendingOrder"), object: nil, userInfo: orderIdd)

    }
    washHub.on("NoAnswer") { args in
        print("-----------------------------------------------Wait")
        print(args)
        let orderId = (args?[0])!
        let orderIdd:[String: Int] = ["orderId": orderId as! Int]
        DispatchQueue.once(token: "\(orderId)", block: {
            if Languages.currentAppleLanguage() == "ar" {
                
                
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                    
                   NotificationCenter.default.post(name: Notification.Name("wait"), object: nil, userInfo: orderIdd)
                    
                }        )
            }else {
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                    
                    NotificationCenter.default.post(name: Notification.Name("wait"), object: nil, userInfo: orderIdd)
                    
                }        )
            }
        })
        
        
        
    }
    washHub.on("NoAnswerr") { args in
        print("-----------------------------------------------Wait")
        print(args)
        let orderId = (args?[0])!
        let orderIdd:[String: Int] = ["orderId": orderId as! Int]
        if Languages.currentAppleLanguage() == "ar" {
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                
                _ = SweetAlert().showAlert(NSLocalizedString("طلب رقم\(orderId)", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("لا يوجد سائقين", comment: "Localize me Label in the ConfirmVC scene"), style: .warning)
                //    NotificationCenter.default.post(name: Notification.Name("PendingOrder"), object: nil)
                
                NotificationCenter.default.post(name: Notification.Name("wait"), object: nil, userInfo: orderIdd)
                
            }        )
        }else {
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                
                _ = SweetAlert().showAlert(NSLocalizedString("Order No.\(orderId) ", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("No driver", comment: "Localize me Label in the ConfirmVC scene"), style: .warning)
                
                //    NotificationCenter.default.post(name: Notification.Name("PendingOrder"), object: nil)
                
                
                NotificationCenter.default.post(name: Notification.Name("wait"), object: nil, userInfo: orderIdd)
                
            }        )
        }
        
        
    }
    washHub.on("DriverUpdateItems") { args in
        print("-----------------------------------------------DriverUpdateItems")
        print(args)
        let orderId = (args?[0])!
        let orderIdd:[String: Int] = ["orderId": orderId as! Int]
        NotificationCenter.default.post(name: Notification.Name("DriverUpdateItems"), object: args, userInfo: orderIdd)
        //    let orderIdd:[String: Int] = ["orderId": orderId as! Int]
        //        if Languages.currentAppleLanguage() == "ar" {
//           
//        }else {
//            
//        }
        
        
    }
    washHub.on("orderapproved") { args in
        
        if Languages.currentAppleLanguage() == "ar" {
        print("-----------------------------------------------OrderApproved")
//        _ = SweetAlert().showAlert(NSLocalizedString("الطلب", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("تم قبول الطلب", comment: "Localize me Label in the ConfirmVC scene"), style: .success)
       print(args)
        }else {
            print("-----------------------------------------------OrderApproved")
//            _ = SweetAlert().showAlert(NSLocalizedString("Order", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("Order Approved", comment: "Localize me Label in the ConfirmVC scene"), style: .success)
            print(args)
        }
        let orderId = (args?[0])!
        let orderIdd:[String: Int] = ["orderId": orderId as! Int]
        NotificationCenter.default.post(name: Notification.Name("OrderApproved"), object: args, userInfo: orderIdd)
        

    }
    washHub.on("OrderRejected") { args in
        if Languages.currentAppleLanguage() == "ar" {
        _ = SweetAlert().showAlert(NSLocalizedString("الطلب", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("تم رفض الطلب", comment: "Localize me Label in the ConfirmVC scene"), style: .error)
        print("-----------------------------------------------OrderRejected")
        print(args)
        NotificationCenter.default.post(name: Notification.Name("OrderRejected"), object: nil)
        }else {
            _ = SweetAlert().showAlert(NSLocalizedString("Order", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("Order Rejected", comment: "Localize me Label in the ConfirmVC scene"), style: .error)
            print("-----------------------------------------------OrderRejected")
            print(args)
            NotificationCenter.default.post(name: Notification.Name("OrderRejected"), object: nil)
        }
        
    }
    washHub.on("DriverComing") { args in
        
        if Languages.currentAppleLanguage() == "ar" {

//        _ = SweetAlert().showAlert(NSLocalizedString("الطلب", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("السائق قادم", comment: "Localize me Label in the ConfirmVC scene"), style: .success)
        print("-----------------------------------------------DriverComing")
        print(args)
//        NotificationCenter.default.post(name: Notification.Name("OrderRejected"), object: nil)
        }else {
//            _ = SweetAlert().showAlert(NSLocalizedString("Order", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("Driver Coming", comment: "Localize me Label in the ConfirmVC scene"), style: .success)
//            print("-----------------------------------------------DriverComing")
//            print(args)
//            NotificationCenter.default.post(name: Notification.Name("OrderRejected"), object: nil)
        }
        print("-----------------------------------------------DriverComing")
        //            print(args)
    }
    washHub.on("driverUpdatee") { args in
        print("-----------------------------------------------driverUpdate")
        print(args)
        
        
        let orderId = args?[0]
        let status = args?[1]
//        let driverID = args?[2]
        let lat = args?[2]
        let lng = args?[3]
        //    NotificationCenter.default.post(name: Notification.Name("PendingOrder"), object: nil)
        
        let orderIdd:[String: AnyObject] = ["orderId": orderId as AnyObject,"status": status as AnyObject,"lat": lat as AnyObject,"lng":lng as AnyObject,"driverID": 0 as AnyObject ]
        NotificationCenter.default.post(name: Notification.Name("driverUpdate"), object: nil,userInfo: orderIdd)
        
    }
    washHub.on("DriverHere") { args in
        let orderId = args?[0]
        let driverPhone = args?[1]
        //        let driverID = args?[2]
        let driverName = args?[2]
       
        //    NotificationCenter.default.post(name: Notification.Name("PendingOrder"), object: nil)
        
        let orderIdd:[String: AnyObject] = ["orderId": orderId as AnyObject,"driverPhone": driverPhone as AnyObject,"driverName": driverName as AnyObject]
        NotificationCenter.default.post(name: Notification.Name("DriverHere"), object: nil,userInfo: orderIdd)
        print(args)

    }
    washHub.on("finishservice") { args in
        
        print(args)
//        NotificationCenter.default.post(name: Notification.Name("DriverHere"), object: nil)
        
    }

    washHub.on("MoneyConfirm") { args in
        print(args)
        NotificationCenter.default.post(name: Notification.Name("MoneyConfirm"), object: nil)
        
    }
    washHub.on("OrderCanceled") { args in
        
        print(args)
        NotificationCenter.default.post(name: Notification.Name("OrderRejected"), object: nil)
        
    }

    connection.addHub(washHub)
    //connection.createHubProxy("washHub")
    let acc = UserDefaults.standard.object(forKey: "AccessToken") as! String
    let aut = ["Authorization": "Bearer \(acc)"]
    print("AccessToken",aut)
    connection.headers = aut

// SignalR events

connection.starting = {
     print("Starting...")
   
}

connection.reconnecting = {
    print("Reconnecting...")
   
}

connection.connected = {
    print("Connection ID: \(self.connection.connectionID)")
    do {
        try HubConnection.washHub.invoke("clientConnected", arguments: [])
    } catch {
        print(error)
        connection.start()

    }
   }

connection.reconnected = {
    print("Reconnected. Connection ID: \(self.connection.connectionID)")
    do {
        try HubConnection.washHub.invoke("clientConnected", arguments: [])
    } catch {
        print(error)
        connection.start()

    }
    
}

connection.disconnected = { 
   print("Disconnected")
    self.connection.connect()
   
}

connection.connectionSlow = { print("Connection slow...") }

connection.error = {  error in
    print("Error: \(error)")
    
    // Here's an example of how to automatically reconnect after a timeout.
    //
    // For example, on the device, if the app is in the background long enough
    // for the SignalR connection to time out, you'll get disconnected/error
    // notifications when the app becomes active again.
    
    if let source = error?["source"] as? String, source == "TimeoutException" {
        print("Connection timed out. Restarting...")
        self.connection.start()
    }
}

connection.start()
        }
}
