//
//  PricingVC.swift
//  Laundriya
//
//  Created by Yo7ia on 5/8/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//


import UIKit
import CCMPopup

class PricingVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet var tableView: UITableView!
    var PricingList = [PriceListModel]()
    @IBOutlet weak var icon: UIButton!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.handleLocalizationChange()
        if Languages.currentAppleLanguage() == "ar"
        {
            if let _img = icon.currentImage {
                icon.setImage( UIImage(cgImage: _img.cgImage!, scale:_img.scale , orientation: UIImageOrientation.upMirrored), for: .normal)
            }
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.automaticallyAdjustsScrollViewInsets = false
        tableView.contentInset = UIEdgeInsets.zero
        
        GetCategories()
    }
    
    func GetCategories()
    {
        Networking.GetCategories { (Categories) in
            
            let services  = Categories.services ?? []
            let items = Categories.Items ?? []
            let servicetype = Categories.servicestypes ?? []
            let prices = Categories.prices ?? []
            
            for i in prices
            {
                let d = PriceListModel(JSON: ["":""])
                d!.Price = i.Price!
                let item = items.filter{$0.id == i.Item}
                for x in item
                {
                    d!.ItemId = x.id!
                    d!.ItemArabicName = x.arabicName!
                    d!.ItemEnglishName = x.englishName!
                }
                let service = services.filter{$0.Id == i.serviceId}
                for x in service
                {
                    d!.ServiceId = x.Id!
                    d!.ServiceArabicName = x.ArabicName!
                    d!.ServiceEnglishName = x.EnglishName!
                }
                let serviceType = servicetype.filter{$0.Id == i.serviceTypeId}
                for x in serviceType
                {
                    d!.serviceTypeId = x.Id!
                    d!.ServiceTypeArabicName = x.ArabicName!
                    d!.ServiceTypeEnglishName = x.EnglishName!
                }
                
                
                self.PricingList.append(d!)
                
            }
            self.tableView.reloadData()
            ProgressUtility.dismissProgressView()
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func Cancel()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Table view data source
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return PricingList.count+1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PricingItemCell", for: indexPath) as! PricingItemCell
        if indexPath.row > 0
        {
            let order = PricingList[indexPath.row-1]
            cell.order = order
            cell.SetupCell()
        }
        else
        {
        }
        return cell
    }
    
    
    
}
