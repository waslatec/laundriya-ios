//
//  OrderItemsConfirm.swift
//  Laundriya
//
//  Created by Yo7ia on 5/9/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//



import UIKit

class  OrderItemsConfirm: UIViewController {
    
    static var orderId = 0
    var totalcost = 0.0
    @IBOutlet weak var totalCost: UILabel!
    @IBOutlet weak var totalCostLabel: UILabel!
    @IBOutlet weak var drivertakeItemBut: UIButton!
    
    @IBOutlet weak var orderCancel: UIButton!
    @IBOutlet weak var OrderIdLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        UIView.appearance().semanticContentAttribute = .forceLeftToRight

        OrderIdLabel.text = String(describing: OrderItemsConfirm.orderId)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func dissmisVC(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func DriverTakeItem(_ sender: AnyObject) {
        
        do {
            
            try HubConnection.washHub.invoke("ConfirmDriverTakeItems", arguments: [OrderItemsConfirm.orderId]){ (result, error) in
                self.dismiss(animated: true, completion: nil)

                if let e = error {
                    print("Error: \(e)")
                } else {
                    print("Success!")
                    // NotificationCenter.default.addObserver(self, selector: #selector(OrderNowVC.methodOfReceivedNotification(notification:)), name: Notification.Name("PendingOrder"), object: nil)
                    // self.performSegue(withIdentifier: "Now", sender: self)
                    if let r = result {
                        print("Result: \(r)")
                    }
                }
            }
        } catch {
            print(error)
        }
    }
    
    @IBAction func CancelOrder(_ sender: AnyObject) {
        do {
            
            try HubConnection.washHub.invoke("cancelorder", arguments: [OrderItemsConfirm.orderId]){ (result, error) in
                self.dismiss(animated: true, completion: nil)

                if let e = error {
                    print("Error: \(e)")
                } else {
                    print("Success!")
                    // NotificationCenter.default.addObserver(self, selector: #selector(OrderNowVC.methodOfReceivedNotification(notification:)), name: Notification.Name("PendingOrder"), object: nil)
                    // self.performSegue(withIdentifier: "Now", sender: self)
//                    self.dismiss(animated: true, completion: nil)
                    
                    if let r = result {
                        print("Result: \(r)")
                    }
                }
            }
            
        } catch {
            print(error)
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
}
