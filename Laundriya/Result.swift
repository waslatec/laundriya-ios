//
//  Result.swift
//  Phunation
//
//  Created by AMIT Developer on 10/24/16.
//  Copyright © 2016 AMIT Software. All rights reserved.
//

import Foundation


enum Result<T, E> {
    case success(T)
    case error(E)
    
    var isSuccess: Bool {
        switch self {
        case .success(_): return true
        case .error(_): return false
        }
    }
}
