//
//  SearchDriversVC.swift
//  Laundriya
//
//  Created by Yo7ia on 5/3/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//


import UIKit
import CCMPopup
import SideMenu

class SearchDriversVC: UIViewController {
    
    var orderData = [Any]()
    var successfullCompletionHandler: ((String) -> Void)?
    var failureCompletionHandler: ((String) -> Void)?
    var cc = [CategoryDTO]()
    var isOrderNow = true
    var ViewWidth = 4.0
    @IBOutlet weak var mincharge: UILabel!
    @IBOutlet weak var outerRippleView: UIView!
    @IBOutlet weak var outerRippleView2: UIView!
    @IBOutlet weak var outerRippleView3: UIView!
    @IBOutlet weak var outerRippleView4: UIView!
    var minscale = 0.2
    var maxscale = 1.3
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        if isOrderNow
        {
            mincharge.text = Languages.currentAppleLanguage() == "en" ? "Searching for drivers" : "جاري البحث عن سائقين"
            minscale = 0.3
            maxscale = 0.7
            ViewWidth = 13.0
            DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
                // do stuff 42 seconds later
                self.setupViewsForRippleEffect(view: self.outerRippleView4,delay: 0.5)
                self.setupViewsForRippleEffect(view: self.outerRippleView,delay: 0.7)
                self.setupViewsForRippleEffect(view: self.outerRippleView2,delay: 0.9)
                self.setupViewsForRippleEffect(view: self.outerRippleView3,delay: 1.1)
                
            }

        }
        else
        {
            mincharge.text = Languages.currentAppleLanguage() == "en" ? "Processing your order" : "جاري الطلب"
            minscale = 0.3
            maxscale = 0.3
            ViewWidth = 35
            DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
                // do stuff 42 seconds later
                self.setupViewsForRippleEffect(view: self.outerRippleView4,delay: 0.5)
                self.setupViewsForRippleEffect(view: self.outerRippleView,delay: 0.7)
                self.setupViewsForRippleEffect(view: self.outerRippleView2,delay: 0.9)
                self.setupViewsForRippleEffect(view: self.outerRippleView3,delay: 1.1)
                
            }

        }
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
//            // do stuff 42 seconds later
//            self.setupViewsForRippleEffect(view: self.outerRippleView,delay: 1.0)
//            
//        }
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
//            // do stuff 42 seconds later
//            self.setupViewsForRippleEffect(view: self.outerRippleView2,delay: 0.7)
//
//        }
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
//            // do stuff 42 seconds later
//            self.setupViewsForRippleEffect(view: self.outerRippleView3,delay: 0.4)
//            
//        }
//        setupViewsForRippleEffect(view: outerRippleView3,delay: 0)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(SearchDriversVC.NoDriversAvailableResponse(notification:)), name: Notification.Name("wait"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SearchDriversVC.OrderConfirmedResponse(notification:)), name: Notification.Name("OrderApproved"), object: nil)
        
        do {
            try HubConnection.washHub.invoke("postOrder", arguments: orderData){ (result, error) in
                
                if let e = error {
                    print("Error: \(e)")
                } else {
                    print("Success!")
                    if let r = result {
                        print("Result: \(r)")
                    }
                }
            }
        } catch {
            print(error)
        }

        
    }
    func setupViewsForRippleEffect(view :UIView , delay: TimeInterval){
        view.layer.zPosition = 1111
        view.layer.cornerRadius = view.frame.size.width / 2;
        view.clipsToBounds = true
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.borderWidth = CGFloat(ViewWidth)
        animateRippleEffect(view :view , delay: delay,delay2: delay)
    }
    func animateRippleEffect(view :UIView , delay: TimeInterval,delay2: TimeInterval){

        view.transform = CGAffineTransform(scaleX: CGFloat(delay2), y: CGFloat(delay2))
        view.alpha = 1
        UIView.animate(withDuration: isOrderNow ? 1.7 : 1.7, delay: delay,
                                   options: UIViewAnimationOptions.curveLinear,
                                   animations: {
                                    view.alpha = self.isOrderNow ? 0 : 0.4
                                    view.transform = CGAffineTransform(scaleX: CGFloat(self.maxscale+delay2), y: CGFloat(self.maxscale+delay2))
        }, completion: { finished in
            self.animateRippleEffect(view :view , delay: 0,delay2: delay2)
        })
        
    }
    func NoDriversAvailableResponse(notification: Notification){
        delay(time: isOrderNow ? 2 : 4) {
            if let orderId = notification.userInfo?["orderId"] as? Int {
                print(orderId)
                self.failureCompletionHandler!("\(orderId)")
                NotificationCenter.default.removeObserver(self)
            }
        }
        
    }
    func OrderConfirmedResponse(notification: Notification){
        
        delay(time: isOrderNow ? 2 : 4) {
            if let orderId = notification.userInfo?["orderId"] as? Int {
                print(orderId)
                self.successfullCompletionHandler!("\(orderId)")
                NotificationCenter.default.removeObserver(self)
            }
        }
       
        
    }

    @IBAction func Cancel()
    {
        self.dismissAnimated()
    }
    
    
}
