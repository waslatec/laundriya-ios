//
//  testHup.swift
//  Washer-Client
//
//  Created by Amr on 2/5/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import UIKit
import SwiftR

class testHup {
    static  var simpleHub: Hub!
    static  var hubConnection =  SignalR("http://washingservice.wasltec.com")
    
    static  func startConnection()
    {
       hubConnection.transport = .auto
       // hubConnection.useWKWebView = true
        hubConnection.signalRVersion = .v2_2_1
            // Hubs...
             hubConnection.connect() {
                //  connection.queryString = ["foo": "bar"]
              //  connection.headers = ["Authorization": "Bearer \(NSUserDefaults.standardUserDefaults().objectForKey("token")!)"]
                
               // print("token=\(NSUserDefaults.standardUserDefaults().objectForKey("token")!)")
                
                
              //   This only works with WKWebView on iOS >= 9
                UserDefaults.standard.register(defaults: ["UserAgent": "SwiftR iOS Demo App"])
                //  hubConnection.customUserAgent = "SwiftR iOS Demo App"

                simpleHub = hubConnection.createHubProxy("washHub")
                hubConnection.addHub(simpleHub)
                hubConnection.start()
               // simpleHub = Hub("washHub")
//                simpleHub.on("ok") { args in
////                    SwiftOverlays.removeAllBlockingOverlays();
////                    
////                    SweetAlert().showAlert("المنتج غير متاح", subTitle:"تم رفض الطلب " , style: AlertStyle.Error);
////                    
////                    NSNotificationCenter.defaultCenter().postNotificationName("RejectedOrder", object: nil)
//                    let message = args![0] as! String
//                    print("@@@@@@@@@@@@@@","\(args)")
//                    print("$$$$$$$$$$$$$$","\(message)")
//                    print("testestestes")
//                    
////                    self.arlt.showInfo("test work fine","yoeeeeeeeeee")
//                }
                
                simpleHub.on("dump") { args in
                    
                    //  print("dump =\(args)")
                    
                }
                
                simpleHub.on("OrderCooked") { args in
//                    SweetAlert().showAlert("", subTitle:"الطلب جاهز للتوصيل" , style: AlertStyle.Success);
//                    
//                    NSNotificationCenter.defaultCenter().postNotificationName("StateChanged", object: nil)
                    
                }
                
                simpleHub.on("orderConfirmed") { args in
//                    SweetAlert().showAlert("تم تأكيد الطلب", subTitle:"ونقله الى المطبخ" , style: AlertStyle.Success);
//                    
//                    NSNotificationCenter.defaultCenter().postNotificationName("StateChanged", object: nil)
                    
                    
                }
                
                simpleHub.on("OrderStartDelivering") { args in
//                    SweetAlert().showAlert("", subTitle:"طلبك فى الطريق اليك", style: AlertStyle.Success);
//                    
//                    NSNotificationCenter.defaultCenter().postNotificationName("StateChanged", object: nil)
                    
                }
                
                simpleHub.on("OrderDeliveried") { args in
//                    SweetAlert().showAlert("", subTitle:"تم توصيل الطلب", style: AlertStyle.Success);
//                    
//                    NSNotificationCenter.defaultCenter().postNotificationName("StateChanged", object: nil)
                    
                }
                
                simpleHub.on("driverUpdate") { args in
                    print("yyyyyyyyyyyyyyyy =\(args)")
                }
                
                // SignalR events
                
                hubConnection.starting = {
                    print("Starting...")
                    
                }
                
                hubConnection.reconnecting = {
                    print("Reconnecting...")
                    
                }
                
                hubConnection.connected = {
                    print("Connected. Connection ID: \(hubConnection.connectionID!)")
                    simpleHub.on("ok") { args in
                        //                    SwiftOverlays.removeAllBlockingOverlays();
                        //
                        //                    SweetAlert().showAlert("المنتج غير متاح", subTitle:"تم رفض الطلب " , style: AlertStyle.Error);
                        //
                        //                    NSNotificationCenter.defaultCenter().postNotificationName("RejectedOrder", object: nil)
                        let message = args![0] as! String
                        print("@@@@@@@@@@@@@@","\(args)")
                        print("$$$$$$$$$$$$$$","\(message)")
                        print("testestestes")
                        
                        //                    self.arlt.showInfo("test work fine","yoeeeeeeeeee")
                    }
                    
                    do {
                        try testHup.simpleHub.invoke("test", arguments: []) { (result, error) in
                            if let e = error {
                                print("Error: \(e)")
                            } else {
                                print("Success!!!!!!!!")
                                if let r = result {
                                    print("Result: \(r)")
                                }
                            }
                        }
                    }catch {
                        print(error)
                    }
                                   }
                
                hubConnection.reconnected = {
                    print("Reconnected. Connection ID: \(hubConnection.connectionID!)")
                    do {
                        try testHup.simpleHub.invoke("test", arguments: []) { (result, error) in
                            if let e = error {
                                print("Error: \(e)")
                            } else {
                                print("Success!!!!!!!!")
                                if let r = result {
                                    print("Result: \(r)")
                                }
                            }
                        }
                    }catch {
                        print(error)
                    }
                    simpleHub.on("ok") { args in
                        //                    SwiftOverlays.removeAllBlockingOverlays();
                        //
                        //                    SweetAlert().showAlert("المنتج غير متاح", subTitle:"تم رفض الطلب " , style: AlertStyle.Error);
                        //
                                          NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RejectedOrder"), object: nil)
                        let message = args![0] as! String
                        print("@@@@@@@@@@@@@@","\(args)")
                        print("$$$$$$$$$$$$$$","\(message)")
                        print("testestestes")
                        
                        //                    self.arlt.showInfo("test work fine","yoeeeeeeeeee")
                    }
                }
                
                hubConnection.disconnected = {
                    print("Disconnected.")
                    hubConnection.start()
                    
                    
                }
                
                hubConnection.connectionSlow = { print("Connection slow...") }
                
                hubConnection.error = { error in
                    print("Error: \(error!)")
                    
                    // Here's an example of how to automatically reconnect after a timeout.
                    //
                    // For example, on the device, if the app is in the background long enough
                    // for the SignalR connection to time out, you'll get disconnected/error
                    // notifications when the app becomes active again.
                    
                    if let source = error?["source"] as? String , source == "TimeoutException" {
                        print("Connection timed out. Restarting...")
                        hubConnection.start()
                    }
                }
            }
            
            
        }
}
        
        /*
         SwiftR.connect("http://nagdy.service.wasltec.com", connectionType: .Persistent) { connection in
         connection.received = { data in
         print("sssssssssssssss=\(data!)")
         }
         }
         */

