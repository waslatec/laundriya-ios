//
//  ProfileVC.swift
//  Laundriya
//
//  Created by Yo7ia on 5/7/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//



import UIKit
import Kingfisher

class ProfileVC: UIViewController {
    
    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var userName: UITextField!
    
    @IBOutlet weak var updateBut: UIButton!
    @IBOutlet weak var icon: UIButton!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.handleLocalizationChange()
        if Languages.currentAppleLanguage() == "ar"
        {
            if let _img = icon.currentImage {
                icon.setImage( UIImage(cgImage: _img.cgImage!, scale:_img.scale , orientation: UIImageOrientation.upMirrored), for: .normal)
            }
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        if Languages.currentAppleLanguage() == "ar" {
            updateBut.setTitle("تحديث", for: .normal)
        }
        
        if let userimage = UserDefaults.standard.object(forKey: "strPictureURL") as! String? {
            let placeholder  = UIImage(named: "placeholder")
            
            let userimage = userimage
            let url = URL(string: userimage)
            userImage.kf.indicator?.startAnimatingView()
            
            userImage.kf.setImage(with: url, placeholder: placeholder, options: .none, progressBlock: nil, completionHandler: nil)
            userImage.kf.indicator?.stopAnimatingView()
            
            userName.text = UserDefaults.standard.object(forKey: "strFirstName") as! String?
            userEmail.text = UserDefaults.standard.object(forKey: "strEmail") as! String?
        }else{
            Networking.GetProfile(completion: { (UserProfile) in
                self.userName.text = UserProfile.UserName
                self.userEmail.text = UserProfile.Email
            })
            
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func CancelClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func updateProfile(_ sender: AnyObject) {
        let name = self.userName.text
        let email = self.userEmail.text
        Networking.UpdateProfile(Name: name!, Email: email!) { (UserProfile) in
            _ = SweetAlert().showAlert(NSLocalizedString("حسابي", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("تم التعديل", comment: "Localize me Label in the ConfirmVC scene"), style: .success)
        }
        
    }
    
   }
