//
//  SideMenuRootVC.swift
//  Washer-Client
//
//  Created by Amr on 2/7/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import UIKit
import SideMenu

class SideMenuRootVC: UITableViewController {
   
    let MenuTitleA = ["الرئيسية",
        "متابعة الطلب"
        ,"طلباتي"
        ,"برومو كود"
       , "الأسعار"
        ,"الإعدادات"]
    let MenuTitleE = ["Home","Track Order","Order History","Promo code","Pricing","Settings"]
    let MenuImage = [#imageLiteral(resourceName: "laundriya-icons_73"),#imageLiteral(resourceName: "laundriya-icons_27"),#imageLiteral(resourceName: "laundriya-icons_35"),#imageLiteral(resourceName: "laundriya-icons_52"),#imageLiteral(resourceName: "laundriya-icons_64"),#imageLiteral(resourceName: "laundriya-icons_91")]

    override func viewDidLoad() {
        super.viewDidLoad()

       print("hiiiiiiiiiiiiiiiii")
       tableView.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return MenuTitleA.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuItemCell", for: indexPath) as! ItemMenuCell

         if Languages.currentAppleLanguage() == "en" {
            SideMenuManager.self.menuRightNavigationController?.leftSide = false
            cell.itemName.text = MenuTitleE[indexPath.row]
            cell.itemMenuImage.image = MenuImage[indexPath.row]
         }else {
            SideMenuManager.self.menuRightNavigationController?.leftSide = false

            cell.itemName.text = MenuTitleA[indexPath.row]
            cell.itemMenuImage.image = MenuImage[indexPath.row]
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0
        {
            SideMenuManager.menuRightNavigationController?.dismissAnimated()
        }
        else if indexPath.row == 1
        {
            self.performSegue(withIdentifier: "TrackOrdersVC", sender: self)
        }
        else if indexPath.row == 2
        {
            self.performSegue(withIdentifier: "myorders", sender: self)
            
        }else if indexPath.row == 4 {
            self.performSegue(withIdentifier: "pricing", sender: self)
            
        }
        else if indexPath.row == 5 {
            self.performSegue(withIdentifier: "settings", sender: self)
            
        }
        }
    

    
    
}
 


