//
//  AddAddressVC.swift
//  Laundriya
//
//  Created by Yo7ia on 5/7/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//



import UIKit
import CCMPopup
import SideMenu

class AddAddressVC: UIViewController {
    
    var successfullCompletionHandler: ((Void) -> Void)?
    var userAddrees : lookupAddress?

    @IBOutlet weak var mylocation: UILabel!
    @IBOutlet weak var mylocaitonHint: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        mylocation.text = userAddrees!.formateer
        
        
    }
    @IBAction func Proceed()
    {
        let DeliveryAddress = self.userAddrees?.formateer
        let AddressHint = mylocaitonHint.text
        let latt = self.userAddrees?.lat
        let lng = self.userAddrees?.lng
        
        Networking.ِAddAddress(DeliveryAddress: DeliveryAddress!, AddressHint: AddressHint, FloorNumber: 0, FlatNumber: 0, Latitude: latt!, Logitude: lng!) { (AddressesList) in
        }
        self.successfullCompletionHandler!()


    }
    @IBAction func Cancel()
    {
        self.dismissAnimated()
    }
    
    
}
