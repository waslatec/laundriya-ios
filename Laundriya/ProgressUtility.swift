//
//  ProgressUtility.swift
//  Le Cadeau
//
//  Created by Mohammad Shaker on 1/21/16.
//  Copyright © 2016 AMIT-Software. All rights reserved.
//

import UIKit
import Foundation
import SVProgressHUD


class ProgressUtility {
    
    class func setProgressViewStyles() {
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.custom)
        SVProgressHUD.setDefaultMaskType(.clear)
        
        SVProgressHUD.setBackgroundColor(UIColor(fromARGBHexString: "379AE1"))
        SVProgressHUD.setForegroundColor(UIColor(fromARGBHexString: "EFEFF4"))
    }
    
    class func showProgressView() {
        DimUtility.addDimView()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
    }
    
    class func showProgressViewWithProgress(_ progress: Float) {
        DispatchQueue.main.async {
            SVProgressHUD.showProgress(progress)
        }
    }
    
    class func dismissProgressView() {
        DimUtility.removeDimView()
        
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
}
