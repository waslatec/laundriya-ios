//
//  SignUpVC.swift
//  Washer-Client
//
//  Created by Amr on 2/2/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import ObjectMapper


class SignUpVC: UIViewController {

    var fullName = ""
    var email = ""
    @IBOutlet var  firstName: UITextField!
    @IBOutlet var  lastName: UITextField!
    @IBOutlet var  EmailAddress: UITextField!
   

     var ffbDetails : facebookInfo?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()

    }
    

    @IBAction func facebookButton(_ sender: AnyObject) {
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager .logIn(withReadPermissions: ["email"], handler: { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData()
                    fbLoginManager.logOut()
                }
            }
        })
    }
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    ProgressUtility.showProgressView()
                    let fbDetails = result  as! [String : AnyObject]
                    let strFirstName: String = fbDetails["first_name"] as! String
                    let strLastName: String = fbDetails["last_name"] as! String
                    let profilePictureURLStr = (fbDetails["picture"]!["data"]!! as! [String : AnyObject])["url"]!
                    let strEmail : String = fbDetails["email"] as! String

                    print("++++++++++++++++++++++++++++++++++++++++++++++=",profilePictureURLStr,strFirstName,strLastName,strEmail)
                    UserDefaults.standard.set(strFirstName, forKey: "strFirstName")
                     UserDefaults.standard.set(strLastName, forKey: "strLastName")
                    self.fullName = strFirstName+" "+strLastName
                    self.email = strEmail
                     UserDefaults.standard.set(profilePictureURLStr, forKey: "strPictureURL")
                    UserDefaults.standard.set(strEmail, forKey: "strEmail")

                    UserDefaults.standard.synchronize()
                    self.performSegue(withIdentifier: "completeGetdata", sender: self)
                    print(result)
                    
                }
            })
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "completeGetdata"
        {
            let vc = segue.destination as! LoginVC
            vc.fullname = self.fullName
            vc.email = self.email
        }
    }
    @IBAction func Continue()
    {
        if firstName.text?.trimmed.characters.count == 0 || lastName.text?.trimmed.characters.count == 0 || EmailAddress.text?.trimmed.characters.count == 0
        {
            SweetAlert().showAlert("Note!", subTitle: "Fill all fields", style: AlertStyle.warning)
            return
        }
        let regex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
            guard NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: EmailAddress.text) else {
                SweetAlert().showAlert("Note!", subTitle: "Enter valid email (eg: test@gmail.com)", style: AlertStyle.warning)
                return
            }
        self.fullName = firstName.text!+" "+lastName.text!
        self.email = EmailAddress.text!
        self.performSegue(withIdentifier: "completeGetdata", sender: self)
    }
    @IBAction func Cancel()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}



class facebookInfo: Mappable {
    var strFirstName: String? {
        didSet {
//            UserDefaults.standard.set(strFirstName, forKey: "strFirstName")
//            UserDefaults.standard.synchronize()
        }
    }
    var strLastName : String? {
        didSet {
//            UserDefaults.standard.set(strLastName, forKey: "strLastName")
//            UserDefaults.standard.synchronize()
        }
    }
    var strPictureURL : String? {
        didSet {
//            UserDefaults.standard.set(strPictureURL, forKey: "strPictureURL")
//            UserDefaults.standard.synchronize()
        }
    }
    var strEmail : String?{
        didSet {
//            UserDefaults.standard.set(strEmail, forKey: "strEmail")
//            UserDefaults.standard.synchronize()
        }
    }
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        strFirstName <- map["first_name"]
        strLastName <- map["last_name"]
        strPictureURL <- map["picture.data.url"]
        strEmail <- map["email"]
        
    }
}



