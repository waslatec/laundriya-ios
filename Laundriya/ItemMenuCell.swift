//
//  ItemMenuCell.swift
//  Washer-Client
//
//  Created by Amr on 2/7/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import UIKit

class ItemMenuCell: UITableViewCell {
    
    @IBOutlet weak var itemMenuImage: UIImageView!
    
    @IBOutlet weak var itemName: UILabel!



}
