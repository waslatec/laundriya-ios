//
//  CategoiesVC.swift
//  Washer-Client
//
//  Created by Amr on 2/1/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import UIKit
import SideMenu
import Kingfisher
import DropDown
import CoreLocation
import CCMPopup

class HomeVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource ,CategoriesCellDelagete{
    
    // var menuButton = UIBarButtonItem()
    let cloths = UIImage(named: "cloths")
    let asas = UIImage(named: "Asas")
    var images:[UIImage] = []
    var imagePath: String?
    var catids :[Int] = []
    var Categories = [CategoryDTO]()
    var selectedCats = [CategoryDTO]()
    var mapVc : MapVC?
    var userLocation : ((lookupAddress) -> ())?
    var userCurrentLocation : lookupAddress?
    let drop = DropDown()
    var clientAddresses = [Address]()
    var postAddress : Address?
    
    @IBOutlet weak var mylocationBackView : UIView!
    @IBOutlet weak var mylocationBTN : UIButton!
    @IBOutlet weak var mylocationaddress : UILabel!
    @IBOutlet weak var PickUpNowBTN : UIButton!
    @IBOutlet weak var PickUpLaterBTN : UIButton!
    
    @IBOutlet weak var collectionView : UICollectionView!
    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.OrderItemsUpdated(notification:)), name: Notification.Name("DriverUpdateItems"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.DriverHere(notification:)), name: Notification.Name("DriverHere"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.OrderRejected(notification:)), name: Notification.Name("OrderRejected"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.OrderMoneyConfirm(notification:)), name: Notification.Name("MoneyConfirm"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.DriverUpdated(notification:)), name: Notification.Name("driverUpdate"), object: nil)
        mylocationBackView.Curvyimage()
        PickUpNowBTN.Curvyimage()
        PickUpLaterBTN.AddBorderWithColor(radius: 15, borderWidth: 2, color: "01B0F0")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.allowsSelection = true
        self.collectionView.isUserInteractionEnabled = true
        self.collectionView.allowsMultipleSelection = true
        self.collectionView?.reloadData()
        HubConnection.startConnection()
        self.hideKeyboardWhenTappedAround()
        
        GetCategories()
        GetAdresses()
        
        
        
        
        
        ProgressUtility.showProgressView()
        
    }
    
    func GetCategories()
    {
        Networking.GetCategories { (Categories) in
            for (i,element) in (self.collectionView.indexPathsForSelectedItems?.enumerated())!
            {
                
                self.collectionView.deselectItem(at: element, animated: false)
                
            }
            self.imagePath = Categories.imagePath
            self.Categories = Categories.Categories ?? []
            self.collectionView?.reloadData()
            ProgressUtility.dismissProgressView()
            
        }
    }
    
    func GetAdresses()
    {
        Networking.AddressesList { (addressesList) in
            self.clientAddresses = []
            self.clientAddresses.append(contentsOf: addressesList.address ?? [])
            self.setupChooseDropDown()
        }
    }
    
    func DriverUpdated(notification: Notification){
        if let orderId = notification.userInfo?["orderId"] as? Int {
            let status = notification.userInfo?["status"] as? Int
            print(orderId)
            
            if status == 6 || status == 5
            {
                performSegue(withIdentifier: "FinishServiceVC", sender: status)
            }
            
            
        }
    }
    
    
    func DriverHere(notification: Notification){
        if let orderId = notification.userInfo?["orderId"] as? Int
        {
            let driverPhone = notification.userInfo?["driverPhone"] as? String
            let driverName = notification.userInfo?["driverName"] as? String
            delay(time: 1) {
                if Languages.currentAppleLanguage() == "ar" {
                    
                    print("-----------------------------------------------DriverHere")
                    _ = SweetAlert().showAlert(NSLocalizedString("الطلب", comment: "Localize me Label in the ConfirmVC scene")+" : \(orderId)" , subTitle: NSLocalizedString("السائق هنا", comment: "Localize me Label in the ConfirmVC scene")+"\n\nالإسم : \(driverPhone!)"+"\nرقم الجوال : \(driverName!)", style: .success)
                    
                }else {
                    print("-----------------------------------------------DriverHere")
                    _ = SweetAlert().showAlert(NSLocalizedString("Order", comment: "Localize me Label in the ConfirmVC scene")+" : \(orderId)" , subTitle: NSLocalizedString("Driver here", comment: "Localize me Label in the ConfirmVC scene")+"\n\n Name : \(driverPhone!)"+"\n Phone : \(driverName!)", style: .success)
                    
                }
                
            }
            
        }
        
        
    }
    func OrderItemsUpdated(notification: Notification){
        
        delay(time: 1) {
            if let orderId = notification.userInfo?["orderId"] as? Int {
                print(orderId)
                let storyboard = Languages.currentAppleLanguage() == "en" ? UIStoryboard(name: "Main", bundle: nil) : UIStoryboard(name: "MainAr", bundle: nil)
                let mainViewController = storyboard.instantiateViewController(withIdentifier: "sss") as! OrderItemsConfirm
                OrderItemsConfirm.orderId = orderId as! Int
                let popup = CCMPopupTransitioning.sharedInstance()
                popup?.destinationBounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(350), height: CGFloat(350))
                mainViewController.view.layer.cornerRadius = 15
                popup?.dismissableByTouchingBackground = false
                popup?.presentedController = mainViewController
                popup?.presentingController = UIApplication.shared.keyWindow?.rootViewController
                self.navigationController?.present(mainViewController, animated: true, completion: nil)
                
            }
        }
        
        
    }
    func OrderMoneyConfirm(notification: Notification){
        
        delay(time: 1) {
            if Languages.currentAppleLanguage() == "ar" {
                
                print("-----------------------------------------------MoneyConfirm")
                _ = SweetAlert().showAlert(NSLocalizedString("الطلب", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("تم استلام المبلغ", comment: "Localize me Label in the ConfirmVC scene"), style: .success)
                //                print(args)
            }else {
                print("-----------------------------------------------MoneyConfirm")
                _ = SweetAlert().showAlert(NSLocalizedString("Order", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("Money deliverd", comment: "Localize me Label in the ConfirmVC scene"), style: .success)
                //                print(args)
                //  NotificationCenter.default.post(name: Notification.Name("OrderApproved"), object: nil)
            }
            
        }
        
        
    }
    
    func OrderRejected(notification: Notification){
        
        delay(time: 1) {
            //            if let orderId = notification.userInfo?["orderId"] as? Int {
            //                print(orderId)
            //            }
            if Languages.currentAppleLanguage() == "ar" {
                
                print("-----------------------------------------------OrderCanceled")
                _ = SweetAlert().showAlert(NSLocalizedString("الطلب", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("تم الغاء الطلب", comment: "Localize me Label in the ConfirmVC scene"), style: .success)
                //                print(args)
                // NotificationCenter.default.post(name: Notification.Name("OrderApproved"), object: nil)
            }else {
                print("-----------------------------------------------OrderCanceled")
                _ = SweetAlert().showAlert(NSLocalizedString("Order", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("Order Canseled", comment: "Localize me Label in the ConfirmVC scene"), style: .success)
                //                print(args)
                // NotificationCenter.default.post(name: Notification.Name("OrderApproved"), object: nil)
            }
            
        }
        
        
    }
    func setupChooseDropDown() {
        drop.anchorView = self.mylocationaddress
        drop.bottomOffset = CGPoint(x: 0, y: mylocationaddress.bounds.height)
        drop.dataSource = self.clientAddresses.map({ (item) in
            return item.deliveryAddress!
        })
        self.mylocationaddress.text = drop.dataSource.last!
        self.userCurrentLocation!.formateer = drop.dataSource.last!
        // Action triggered on selection
        drop.selectionAction = { [unowned self] (index, item) in
            self.mylocationaddress.text = item
            let address = self.clientAddresses[index]
            self.postAddress = address
            let location = CLLocation(latitude: address.lat!, longitude: address.lng!)
            self.mapVc?.getLocation(location: location)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
    }
    
    func CategorySelected(_ cell: CategoriesCell) {
        
        if cell.isSelected
        {
            self.collectionView.deselectItem(at: cell.index!, animated: true)
        }
        else
        {
            self.collectionView.selectItem(at: cell.index, animated: true, scrollPosition: .top)
            
        }
        cell.configureCell(isSelected: cell.isSelected)
        
    }
    @IBAction func MenuTapped() {
        //        if Languages.currentAppleLanguage() == "en" {
        
        self.present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
        //        }else {
        //            self.present(SideMenuManager.menuLeftNavigationController!,animated: true, completion: nil)
        //        }
    }
    
    
    //    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    //        return CGSize(width: UIScreen.main.bounds.width - 50, height: 204)
    //    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCell", for: indexPath) as! CategoriesCell
        
        let category = Categories[indexPath.row]
        let url = ("\(imagePath!)/\(category.id!).png")
        let imageUrl = URL(string: url)
        
        cell.category = category
        cell.index = indexPath
        cell.delegate = self
        switch indexPath.row {
        case 0:
            cell.categoryImage.image = #imageLiteral(resourceName: "laundriya-icons_115")
        case 1:
            cell.categoryImage.image = #imageLiteral(resourceName: "laundriya-icons_118")
        case 2:
            cell.categoryImage.image = #imageLiteral(resourceName: "laundriya-icons_121")
            
        default:
            cell.categoryImage.kf.setImage(with: imageUrl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
        }
        if Languages.currentAppleLanguage() == "en" {
            
            cell.categoryName.text = category.englishName
            
            
        }else {
            //            cell.categoryImage.kf.setImage(with: imageUrl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
            cell.categoryName.text = category.arabicName
            
        }
        cell.configureCell(isSelected: cell.isSelected)
        
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addAddress" {
            let addAddress = segue.destination as! AddAddressVC
            addAddress.userAddrees = self.userCurrentLocation
            addAddress.successfullCompletionHandler = {
                addAddress.dismiss(animated: true, completion: {
                    self.GetAdresses()
                })
            }
            addAddress.view.layer.cornerRadius = 15
            let popupSegue: CCMPopupSegue? = segue as? CCMPopupSegue
            popupSegue?.destinationBounds = CGRect(x: 0, y: 0, width: 300, height: 300)
            popupSegue?.backgroundViewColor = UIColor(fromARGBHexString: "456DB1")
            popupSegue?.backgroundViewAlpha = 0.9
            popupSegue?.backgroundBlurRadius = 0.1
            popupSegue?.dismissableByTouchingBackground = false
            
        }
        else if segue.identifier == "FinishServiceVC" {
            let finishService = segue.destination as! FinishServiceVC
            finishService.orderID = sender as! Int
            finishService.successfullCompletionHandler = { x in
                finishService.dismiss(animated: true, completion: {
                    do {
                        
                        try HubConnection.washHub.invoke("ClientFinishService", arguments: [x]){ (result, error) in
                            
                            if let e = error {
                                print("Error: \(e)")
                            } else {
                                print("Success!------------------ClientFinishService")
                                self.navigationController?.popToRootViewController(animated: true)
                                _ = SweetAlert().showAlert(Languages.currentAppleLanguage() == "ar" ? "تم بنجاح" : "Success" , subTitle: Languages.currentAppleLanguage() == "إنهاء الخدمة" ? "تم بنجاح" : "Service Finished", style: .success)
                                //  NotificationCenter.default.addObserver(self, selector: #selector(OrderNowVC.methodOfReceivedNotification(notification:)), name: Notification.Name("PendingOrder"), object: nil)
                                
                                if let r = result {
                                    print("Result: \(r)")
                                }
                            }
                        }
                    } catch {
                        print(error)
                    }
                })
            }
            finishService.view.layer.cornerRadius = 15
            
            let popupSegue: CCMPopupSegue? = segue as? CCMPopupSegue
            popupSegue?.destinationBounds = CGRect(x: 0, y: 0, width: 300, height: 300)
            popupSegue?.backgroundViewColor = UIColor(fromARGBHexString: "456DB1")
            popupSegue?.backgroundViewAlpha = 0.9
            popupSegue?.backgroundBlurRadius = 0.1
            popupSegue?.dismissableByTouchingBackground = false
            
        }
            
        else if segue.identifier == "mapVC"{
            
            mapVc = (segue.destination as! MapVC)
            mapVc!.userAddress = { (address) in
                
                if address.lng != nil && address.lat != nil
                {
                    MapTasks.geocodeAddresswithlatlng(lat: address.lat!, lng: address.lng!, completion: { (lookupad) in
                        if (lookupad.resultsAdd?.count)! > 0
                        {
                            self.userCurrentLocation = lookupad.resultsAdd![0]
                            self.mylocationaddress.text = lookupad.resultsAdd![0].formateer
                        }
                    })
                }
                
            }
        }
        else
        {
            
            if segue.identifier == "PickUpNow"
            {
                let vc = segue.destination as! OrderNowVC
                vc.catids = self.catids
                vc.Categories = self.selectedCats
                vc.postAddress = self.postAddress
                vc.userLocation = self.userCurrentLocation
            }
            else if segue.identifier == "PickUpLater"
            {
                let vc = segue.destination as! OrderLaterVC
                vc.catids = self.catids
                vc.Categories = self.selectedCats
                vc.postAddress = self.postAddress
                vc.userLocation = self.userCurrentLocation
            }
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! CategoriesCell
        //        cell.configureCell(isSelected: !cell.isSelected)
    }
    //
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! CategoriesCell
        //                cell.configureCell(isSelected: cell.isSelected)
        
        
    }
    
    @IBAction func PickUpNowClicked()
    {
        if self.postAddress == nil  {
            drop.show()
            _ = SweetAlert().showAlert(NSLocalizedString("خطا", comment: "LocalizedStringin OrderNowVC"), subTitle: NSLocalizedString("اختر عنوان", comment: "LocalizedStringin OrderNowVC"), style: .error)
            //              SwiftOverlays.removeAllBlockingOverlays()
            
        }else{
//            if self.postAddress!.deliveryAddress! == self.userCurrentLocation!.formateer!
//            {
                var data = [CategoryDTO]()
                let d = self.collectionView.indexPathsForSelectedItems!
                for (_, element) in d.enumerated() {
                    
                    data.append(Categories[element.row])
                    catids.append(Categories[element.row].id!)
                    let user = UserDefaults.standard
                    user.set(catids, forKey: "catids")
                    user.synchronize()
                }
                
                if data.count == 0
                {
                    SweetAlert().showAlert(NSLocalizedString("Error", comment: "LocalizedStringin OrderNowVC"), subTitle: NSLocalizedString("Choose at least one category", comment: "LocalizedStringin OrderLaterVC"), style: .error)
                    return
                }
                selectedCats = data
                postAddress!.deliveryAddress = self.mylocationaddress.text!
                performSegue(withIdentifier: "PickUpNow", sender: nil)
//            }
//            else
//            {
//                performSegue(withIdentifier: "addAddress", sender: nil)
//                
//            }
        }
    }
    @IBAction func PickUpLaterClicked()
    {
        if self.postAddress == nil  {
            drop.show()
            _ = SweetAlert().showAlert(NSLocalizedString("خطا", comment: "LocalizedStringin OrderNowVC"), subTitle: NSLocalizedString("اختر عنوان", comment: "LocalizedStringin OrderNowVC"), style: .error)
            //              SwiftOverlays.removeAllBlockingOverlays()
            
        }else{
//            if self.postAddress!.deliveryAddress! == self.userCurrentLocation!.formateer!
//            {
                var data = [CategoryDTO]()
                let d = self.collectionView.indexPathsForSelectedItems!
                for (_, element) in d.enumerated() {
                    
                    data.append(Categories[element.row])
                    catids.append(Categories[element.row].id!)
                    let user = UserDefaults.standard
                    user.set(catids, forKey: "catids")
                    user.synchronize()
                }
                
                if data.count == 0
                {
                    SweetAlert().showAlert(NSLocalizedString("Error", comment: "LocalizedStringin OrderNowVC"), subTitle: NSLocalizedString("Choose at least one category", comment: "LocalizedStringin OrderLaterVC"), style: .error)
                    return
                }
                selectedCats = data
                postAddress!.deliveryAddress = self.mylocationaddress.text!
                performSegue(withIdentifier: "PickUpLater", sender: nil)
//            }
//            else
//            {
//                performSegue(withIdentifier: "addAddress", sender: nil)
//                
//            }
        }
        
        
    }
    @IBAction func mylocationBTNClicked()
    {
        drop.show()
    }
    
    
}
