//
//  APIResponsCode.swift
//  Washer-Client
//
//  Created by Mohamed Namir on 1/27/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import UIKit

public enum APICode: Int {
    
   case BadParameter = 0,
    UnCompleteParamter,
    UnAuthorized,
    Success,
    BadData,
    ServerError,
    NumberIsExist,
    EmailIsExist,
    NumberNotExist,
    ChargeCardNotValid,
    NoUpdates,
    OrderNotExist
    
}

public enum OrderStatus: String {
     case
    Requested = "Requested",
    DriverApproved = "Driver Approved",
    ClientpostItems = "Client post items",
    InProcess = "In Process",
    WashedItemDelivering = "Delivering Washed Items",
    ClientFinishService  = "Client Finish Service",
    DELIVERED  = "DELIVERED"
    
}
public enum OrderStatusArabic: String {
    case
    Requested = "تم الطلب",
    DriverApproved = "تم تعيين السائق",
    ClientpostItems = "تم اختيار المنتجات",
    InProcess = "قيد التنفيذ",
    WashedItemDelivering = "جاري التوصيل",
    ClientFinishService  = "تم انهاء الخدمة",
    DELIVERED  = "تم التوصيل"
    
}
