//
//  APIRequset.swift
//  Washer-Client
//
//  Created by Amr on 1/23/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//



import UIKit
import Alamofire
import ObjectMapper

class Networking {
    
    class func Signup(name:String,email: String,password: String,mobileNumber: String, completion : @escaping (Code) -> () ){
        let url = "http://washingservice.wasltec.com/api/users/register"
        let param = ["Name": name ,"Mobilenumber": mobileNumber,"Email": email,"Password": password] as [String : Any]
        //        let param = ["Mobilenumber": mobileNumber] as [String : Any]
        
        Alamofire.request( url, method: .post , parameters: param ).responseString { response in
            if let JSON = response.result.value {
                ProgressUtility.dismissProgressView()

                completion(Mapper<Code>().map(JSONString: JSON)!)
            } else {
                ProgressUtility.dismissProgressView()

                print("Error")
            }
        }
    }
    
    class func Confirm(code: String,mobileNumber: String, completion : @escaping (Code) -> () ){
        let url = "http://washingservice.wasltec.com/api/users/confirm"
        let param = ["Code": code,"Mobilenumber": mobileNumber] as [String : Any]
        
        Alamofire.request( url, method: .post , parameters: param ).responseString { response in
            if let JSON = response.result.value {
                if JSON != "null" {
//                    ProgressUtility.dismissProgressView()

                    completion(Mapper<Code>().map(JSONString: JSON)!)
                }else{
//                    ProgressUtility.dismissProgressView()

                     _ = SweetAlert().showAlert(NSLocalizedString("Code Check", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("Somthing went worng", comment: "Localize me Label in the ConfirmVC scene"), style: .error)
                }
            } else {
                print("Error")
            }
        }
    }
    
    class func RequestCode(mobileNumber: String, completion : @escaping (Code) -> () ){
        let url = "http://washingservice.wasltec.com/api/users/ForgetPassword"
        let param = ["MobileNumber": mobileNumber] as [String : Any]
        
        Alamofire.request( url, method: .post , parameters: param ).responseString { response in
            if let JSON = response.result.value {
                if JSON != "null" {
                    ProgressUtility.dismissProgressView()

                    completion(Mapper<Code>().map(JSONString: JSON)!)
                }else{
                    ProgressUtility.dismissProgressView()

                    _ = SweetAlert().showAlert(NSLocalizedString("Code Check", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("Somthing went worng", comment: "Localize me Label in the ConfirmVC scene"), style: .error)
                }
            } else {
                ProgressUtility.dismissProgressView()

                print("Error")
            }
        }
    }
    
    
    class func login(mobileNumber : String,password : String ,completion : @escaping (Code) -> () ){
        let url = "http://washingservice.wasltec.com/api/users/login"
        let param = ["username": mobileNumber ,"password": password] as [String : Any]
        Alamofire.request( url, method: .post , parameters: param ).responseString { response in
            if let JSON = response.result.value {
                if JSON != "null" {
                    ProgressUtility.dismissProgressView()

                    completion(Mapper<Code>().map(JSONString: JSON)!)
                }else{
                    ProgressUtility.dismissProgressView()

                    _ = SweetAlert().showAlert(NSLocalizedString("Code Check", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("Somthing went worng", comment: "Localize me Label in the ConfirmVC scene"), style: .error)
                }
            } else {
                ProgressUtility.dismissProgressView()

                print("Error")
            }
        }
    }
//    SyncCatsItems
    class func GetCategories(completion : @escaping (Categories) -> () ){
        let url = "http://washingservice.wasltec.com/api/users/SyncData"
        let accessToken = UserDefaults.standard.object(forKey: "AccessToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(accessToken) "]
        let param = ["SyncDate":"1/11/2011"]
        print(accessToken)
        Alamofire.request( url, method: .post , parameters: param, headers: headers).responseString { response in
            if let JSON = response.result.value {
                if JSON != "null" && JSON != "{\"Message\":\"An error has occurred.\"}"{
                    ProgressUtility.dismissProgressView()

                    completion(Mapper<Categories>().map(JSONString: JSON)!)
                }else{
                     _ = SweetAlert().showAlert(NSLocalizedString("Code Check", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("Somthing went worng", comment: "Localize me Label in the ConfirmVC scene"), style: .error)
                    ProgressUtility.dismissProgressView()

                }
            } else {
                ProgressUtility.dismissProgressView()

                    _ = SweetAlert().showAlert(NSLocalizedString("Code Check", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("Somthing went worng", comment: "Localize me Label in the ConfirmVC scene"), style: .error)            }
        }
        
    }
    
    
    class func GetOrders(completion : @escaping (Orders) -> () ){
        let url = "http://washingservice.wasltec.com/api/users/OrdersList"
        let accessToken = UserDefaults.standard.object(forKey: "AccessToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(accessToken) "]
        print(accessToken)
        
        Alamofire.request( url, method: .post , headers: headers).responseString { response in
            if let JSON = response.result.value {
                if JSON != "null" {
//                    ProgressUtility.dismissProgressView()

                    completion(Mapper<Orders>().map(JSONString: JSON)!)
                }else{
                    ProgressUtility.dismissProgressView()

                     _ = SweetAlert().showAlert(NSLocalizedString("Code Check", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("Somthing went worng", comment: "Localize me Label in the ConfirmVC scene"), style: .error)
                }
            } else {
                print("Error")
            }
        }
    }
    
    class func AddressesList(completion : @escaping (AddressesList) -> () ){
        let url =  "http://washingservice.wasltec.com/api/users/AddressesList"
        let accessToken = UserDefaults.standard.object(forKey: "AccessToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(accessToken) "]
       // print(accessToken)
        ProgressUtility.showProgressView()
        Alamofire.request( url, method: .post , headers: headers).responseString { response in
            if let JSON = response.result.value {
                if JSON != "null" {
                    ProgressUtility.dismissProgressView()

                    completion(Mapper<AddressesList>().map(JSONString: JSON)!)
                }else{
                    ProgressUtility.dismissProgressView()

                     _ = SweetAlert().showAlert(NSLocalizedString("Code Check", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("Somthing went worng", comment: "Localize me Label in the ConfirmVC scene"), style: .error)
                }
            } else {
                print("Error")
            }
        }
        
    }
    class func ِAddAddress(DeliveryAddress: String,AddressHint: String?,FloorNumber: Int?,FlatNumber: Int?,Latitude: Double,Logitude: Double,completion : @escaping (AddressesList) -> () ){
        let url =  "http://washingservice.wasltec.com/api/users/AddAddress"
        let accessToken = UserDefaults.standard.object(forKey: "AccessToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(accessToken) "]
        let param = ["DeliveryAddress": DeliveryAddress,"AddressHint": AddressHint,"FloorNumber": FloorNumber,"FlatNumber": FlatNumber,"Latitude": Latitude,"Logitude": Logitude] as [String : Any]
        print(accessToken)
        Alamofire.request( url, method: .post, parameters: param , headers: headers).responseString { response in
            if let JSON = response.result.value {
                if JSON != "null" {
                    ProgressUtility.dismissProgressView()

                    completion(Mapper<AddressesList>().map(JSONString: JSON)!)
                }else{
                    ProgressUtility.dismissProgressView()

                     _ = SweetAlert().showAlert(NSLocalizedString("Code Check", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("Somthing went worng", comment: "Localize me Label in the ConfirmVC scene"), style: .error)
                }
            } else {
                print("Error")
            }
        }
        
    }
    class func UpdateProfile(Name: String,Email: String,completion : @escaping (UserProfile) -> () ){
        let url =  "http://washingservice.wasltec.com/api/users/UpdateProfile"
        let accessToken = UserDefaults.standard.object(forKey: "AccessToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(accessToken) "]
        let param = ["Name": Name,"Email": Email] as [String : Any]
        print(accessToken)
        Alamofire.request( url, method: .post, parameters: param , headers: headers).responseString { response in
            if let JSON = response.result.value {
                if JSON != "null" {
                    ProgressUtility.dismissProgressView()

                    completion(Mapper<UserProfile>().map(JSONString: JSON)!)
                }else{
                    ProgressUtility.dismissProgressView()

                     _ = SweetAlert().showAlert(NSLocalizedString("Code Check", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("Somthing went worng", comment: "Localize me Label in the ConfirmVC scene"), style: .error)
                }
            } else {
                print("Error")
            }
        }
        
    }
    class func GetProfile(completion : @escaping (UserProfile) -> () ){
        let url =  "http://washingservice.wasltec.com/api/users/Getprofile"
        let accessToken = UserDefaults.standard.object(forKey: "AccessToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(accessToken) "]
       
        print(accessToken)
        Alamofire.request( url, method: .post , headers: headers).responseString { response in
            if let JSON = response.result.value {
                if JSON != "null" {
                    ProgressUtility.dismissProgressView()

                    completion(Mapper<UserProfile>().map(JSONString: JSON)!)
                }else{
                    ProgressUtility.dismissProgressView()

                     _ = SweetAlert().showAlert(NSLocalizedString("Code Check", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("Somthing went worng", comment: "Localize me Label in the ConfirmVC scene"), style: .error)
                }
            } else {
                print("Error")
            }
        }
        
    }
    class func GetorkHours(completion : @escaping (Calendarr) -> () ){
        let url =  "http://washingservice.wasltec.com/api/users/GetWorkHours"
        
        Alamofire.request( url, method: .post ).responseString { response in
            if let JSON = response.result.value {
                if JSON != "null" {
                    ProgressUtility.dismissProgressView()

                completion(Mapper<Calendarr>().map(JSONString: JSON)!)
                }else{
                    ProgressUtility.dismissProgressView()

                    _ = SweetAlert().showAlert(NSLocalizedString("Code Check", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("Somthing went worng", comment: "Localize me Label in the ConfirmVC scene"), style: .error)
                }
            } else {
                print("Error")
            }
        }
        
    }

}


