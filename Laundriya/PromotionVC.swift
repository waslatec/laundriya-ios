//
//  PromotionVC.swift
//  Washer-Client
//
//  Created by Amr on 2/5/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import UIKit
import CCMPopup
import SideMenu

class PromotionVC: UIViewController {
    
    var category = [CategoryDTO]()
    var successfullCompletionHandler: ((Void) -> Void)?

    @IBOutlet weak var mincharge: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        self.hideKeyboardWhenTappedAround()

        mincharge.text = Languages.currentAppleLanguage() == "en" ? "The order minimum amount is \n\n" : "الحد الأدني للطلب : \n\n"
        for i in category
        {
        let min = String(describing: i.min!)

            mincharge.text = "\(mincharge.text!) \(Languages.currentAppleLanguage() == "en" ? i.englishName! : i.arabicName!)  : \(min)\n"
        }

    }
    @IBAction func Proceed()
    {
        self.successfullCompletionHandler?()
    }
    @IBAction func Cancel()
    {
        self.dismissAnimated()
    }
    
    
}
