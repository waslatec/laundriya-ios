//
//  LoginVCViewController.swift
//  Washer-Client
//
//  Created by Amr on 1/25/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import UIKit
import CCMPopup


class LoginVC: UIViewController {
    
    @IBOutlet weak var userNameText: UITextField!
    var fullname = ""
    var email = ""
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ProgressUtility.dismissProgressView()
        self.hideKeyboardWhenTappedAround()
    }
    
       
    
    @IBAction func login(_ sender: AnyObject) {
        self.dismissKeyboard()
        if userNameText.text?.trimmed.characters.count == 0
        {
           SweetAlert().showAlert("Note!", subTitle: "Enter Valid Mobile Number", style: AlertStyle.warning)
            return
        }
        let mobileNumber = "\(userNameText.text!)"
        
        ProgressUtility.showProgressView()

        Networking.Signup(name: fullname, email: email, password: fullname+email, mobileNumber: mobileNumber) { (code) in
            
            if code.code == 3 {
                self.performSegue(withIdentifier: "activeRequset", sender: Any?.self)
                
            }else
            {
                
             _ = SweetAlert().showAlert("Code Check", subTitle: "Somthing went worng", style: AlertStyle.error)
                
              //  alrt.showError("Code Check", subTitle: "Somthing et worng")
                
            }
            
        }
    }
    
   
    @IBAction func Cancel()
    {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "activeRequset"
        {
            let confirmVC = segue.destination as! ConfirmVC
            confirmVC.userName = userNameText.text!
        }
    }
    
}
