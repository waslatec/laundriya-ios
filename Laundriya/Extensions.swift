//
//  Extensions.swift
//  Washer-Client
//
//  Created by Amr on 1/30/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import UIKit
import Foundation
import MapKit
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    func handleLocalizationChange()
    {
//        if Languages.currentAppleLanguage() == "ar"
//        {
//        loopThroughSubViewAndFlipTheImageIfItsAUIImageView(subviews: self.view.subviews)
//        }
        UIView.appearance().semanticContentAttribute = Languages.currentAppleLanguage() == "en" ? .forceLeftToRight : .forceRightToLeft
    }
    func loopThroughSubViewAndFlipTheImageIfItsAUIImageView(subviews: [UIView]) {
        if subviews.count > 0 {
            for subView in subviews {
                if (subView is UIImageView) && subView.tag < 0 {
                    let toRightArrow = subView as! UIImageView
                    if let _img = toRightArrow.image {
                        toRightArrow.image = UIImage(cgImage: _img.cgImage!, scale:_img.scale , orientation: UIImageOrientation.upMirrored)
                    }
                    
                }
               else if (subView is UIButton) && subView.tag < 0 {
                    let toRightArrow = subView as! UIButton
                    if let _img = toRightArrow.currentImage {
                        toRightArrow.setImage(UIImage(cgImage: _img.cgImage!, scale:_img.scale , orientation: UIImageOrientation.upMirrored), for: .normal)
                    }
                    
                }
                loopThroughSubViewAndFlipTheImageIfItsAUIImageView(subviews: subView.subviews)
            }
        }
    }

    func delay(time: Double, closure:@escaping ()->()) {
        
        DispatchQueue.main.asyncAfter(deadline: .now()+time, execute: {
            closure()
        })
        
    }
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
extension String {
    
    func getDateFromString() -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let date = dateFormatter.date(from: self)
        {
            return date
        }
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSZ"
        if let date = dateFormatter.date(from: self)
        {
            return date
        }
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
        if let date = dateFormatter.date(from: self)
        {
            return date
        }
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        if let date = dateFormatter.date(from: self)
        {
            return date
        }
        
        return dateFormatter.date(from: self)!
    }
    func heightForWithFont( _ width: CGFloat) -> CGFloat {
        
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width + 5 + 5, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.text = self
        label.sizeToFit()
        return label.frame.height + 10
    }
    func heightForWithFont() -> CGFloat {
        
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.text = self
        label.sizeToFit()
        return label.frame.height
    }
    var trimmed: String {
        return trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    var toDate: Date {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.long
        
        dateFormatter.timeStyle = DateFormatter.Style.none
        return dateFormatter.date(from: self)!
        
    }
    
    
}
extension Date {
    var asString: String
    {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.long
        
        dateFormatter.timeStyle = DateFormatter.Style.none
        return dateFormatter.string(from: self)
    }
    var asStringWithTime: String
    {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.short
        
        dateFormatter.timeStyle = DateFormatter.Style.short
        return dateFormatter.string(from: self)
    }
    var ToTimeOnly: String
    {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.none
        
        dateFormatter.timeStyle = DateFormatter.Style.short
        return dateFormatter.string(from: self)
    }
    
    func addDaysToCurrentDate(numofDays: Int) -> Date
    {
        let today = self
        return Calendar.current.date(byAdding: .day, value: numofDays, to: today)!
    }
    var trimTime: Date {
        
        
        
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        
        
        //        let components = cal.components([.Day, .Month, .Year], fromDate: self)
        return (cal as NSCalendar).date(bySettingHour: 2, minute: 0, second: 0, of: self, options: NSCalendar.Options())!
    }
    
}
    extension DispatchQueue {
        private static var _onceTracker = [String]()
        
        public class func once(file: String = #file, function: String = #function, line: Int = #line, block:(Void)->Void) {
            let token = file + ":" + function + ":" + String(line)
            once(token: token, block: block)
        }
        func delay(time: Double, closure:@escaping ()->()) {
            
            self.asyncAfter(deadline: .now()+time, execute: { 
                closure()

            })
            
        }
        /**
         Executes a block of code, associated with a unique token, only once.  The code is thread safe and will
         only execute the code once even in the presence of multithreaded calls.
         
         - parameter token: A unique reverse DNS style name such as com.vectorform.<name> or a GUID
         - parameter block: Block to execute once
         */
        public class func once(token: String, block:(Void)->Void) {
            objc_sync_enter(self)
            defer { objc_sync_exit(self) }
            
            
            if _onceTracker.contains(token) {
                return
            }
            
            _onceTracker.append(token)
            block()
        }
    }
extension UIImage {
    
   
    
    func tintPhoto(_ tintColor: UIColor) -> UIImage {
        
        return modifiedImage { context, rect in
            // draw black background - workaround to preserve color of partially transparent pixels
            context.setBlendMode(.normal)
            UIColor.black.setFill()
            context.fill(rect)
            
            // draw original image
            context.setBlendMode(.normal)
            context.draw(cgImage!, in: rect)
            
            // tint image (loosing alpha) - the luminosity of the original image is preserved
            context.setBlendMode(.color)
            tintColor.setFill()
            context.fill(rect)
            
            // mask by alpha values of original image
            context.setBlendMode(.destinationIn)
            context.draw(context.makeImage()!, in: rect)
        }
    }
    func resizeImage(newWidth: CGFloat) -> UIImage? {
        let image = self
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    func maskWithColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!
        
        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        
        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)
        
        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }
    /**
     Tint Picto to color
     
     - parameter fillColor: UIColor
     
     - returns: UIImage
     */
    func tintPicto(_ fillColor: UIColor) -> UIImage {
        
        return modifiedImage { context, rect in
            // draw tint color
            context.setBlendMode(.normal)
            fillColor.setFill()
            context.fill(rect)
            
            // mask by alpha values of original image
            context.setBlendMode(.destinationIn)
            context.draw(cgImage!, in: rect)
        }
    }
    
    /**
     Modified Image Context, apply modification on image
     
     - parameter draw: (CGContext, CGRect) -> ())
     
     - returns: UIImage
     */
    fileprivate func modifiedImage(_ draw: (CGContext, CGRect) -> ()) -> UIImage {
        
        // using scale correctly preserves retina images
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        let context: CGContext! = UIGraphicsGetCurrentContext()
        assert(context != nil)
        
        // correctly rotate image
        context.translateBy(x: 0, y: size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        
        let rect = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
        
        draw(context, rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
}
extension UIView
{
    func roundedimage()
    {
        self.layer.cornerRadius = self.frame.size.width / 2
        //        self.clipsToBounds = true
    }
    func Curvyimage()
    {
        self.layer.cornerRadius = self.frame.size.width / 15
        self.clipsToBounds = true
    }
    func AddBorderWithColor(radius:Int? = 15,borderWidth:Int? = 0,color:String? = "01B0F0" )
    {
        self.layer.cornerRadius = CGFloat(radius!)
        self.layer.borderWidth = CGFloat(borderWidth!)
        self.layer.borderColor = UIColor(fromARGBHexString: color).cgColor
        self.clipsToBounds = true
        
    }
}
extension CLLocation
{
    func DistanceBetweenTwoLocations(lat: Double,lng: Double) -> String {
        let loc = CLLocation(latitude: lat, longitude: lng)
        let distance =  self.distance(from: loc)
        if distance > 1000
        {
            return "\((distance/1000).rounded()) KM ".replacingOccurrences(of: ".0", with: "")
            
        }
        else
        {
            return "\(distance.rounded()) M "
        }
    }
}
extension UITextView
{
    func addPadding() {
        self.contentInset = UIEdgeInsetsMake(5, 5, 5, 5);
        
    }
}
extension UITextField
{
    
    func addPadding() {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.frame.height))
        self.leftView = paddingView
        self.rightView = paddingView
        self.leftViewMode = UITextFieldViewMode.always
        self.rightViewMode = UITextFieldViewMode.always
        
    }
    func showExclamationMark() {
        self.rightViewMode = .always
        let exclamationIV = UIImageView(frame: CGRect(x: 0, y: 0, width: 16, height: 16))
        exclamationIV.image = UIImage(named: "exclamation")
        exclamationIV.contentMode = .scaleAspectFit
        self.rightView = exclamationIV
    }
    
    func hideExclamationMark() {
        self.rightViewMode = .never
        self.rightView = nil
    }
    
    func setBottomBorder(_ color:UIColor)
    {
        self.borderStyle = UITextBorderStyle.none;
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1).cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height+15 - width,   width:  self.frame.size.width, height: self.frame.size.height)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
        //        self.layer.masksToBounds = true
    }
}

