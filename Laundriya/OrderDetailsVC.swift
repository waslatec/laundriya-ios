//
//  OrderDetailsVC.swift
//  Laundriya
//
//  Created by Yo7ia on 5/7/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//


import UIKit
import CCMPopup

class OrderDetailsVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var orders :Order?
    @IBOutlet var category: UILabel!
    @IBOutlet var Date: UILabel!
    @IBOutlet var OrderNumber: UILabel!
    @IBOutlet var orderStatus: UILabel!
    @IBOutlet var DeliveryDate: UILabel!
    @IBOutlet var TotalPrice: UILabel!
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var icon: UIButton!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.handleLocalizationChange()
        if Languages.currentAppleLanguage() == "ar"
        {
            if let _img = icon.currentImage {
                icon.setImage( UIImage(cgImage: _img.cgImage!, scale:_img.scale , orientation: UIImageOrientation.upMirrored), for: .normal)
            }
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.handleLocalizationChange()
        self.hideKeyboardWhenTappedAround()
        self.tableView.delegate = self
        self.tableView.dataSource = self
         category.text = ""
        for i in orders!.Category
        {
            let te = Languages.currentAppleLanguage() == "ar" ? i.arabicName : i.englishName
            if (orders?.Category.count)! > 1
            {
                category.text = category.text! + " , " + te!

            }
            else
            {
                category.text = category.text! + "" + te!

            }
        }
        
        self.automaticallyAdjustsScrollViewInsets = false
        tableView.contentInset = UIEdgeInsets.zero

        Date.text = orders!.Date.replacingOccurrences(of: "T", with: " ")
        DeliveryDate.text = orders!.Date.replacingOccurrences(of: "T", with: " ")
        var Cost = 0
        for i in orders!.items
        {
            Cost += i.amount! * i.unitePrice!
        }
        let curr = Languages.currentAppleLanguage() == "ar" ? "ريال " : " SAR"
        TotalPrice.text = "\(Cost) "+curr
        OrderNumber.text = "\(orders!.Id)"

        switch (orders!.State)
        {
        case 0:
            orderStatus.text = Languages.currentAppleLanguage() == "en" ? OrderStatus.Requested.rawValue : OrderStatusArabic.Requested.rawValue
            
        case 1:
           orderStatus.text = Languages.currentAppleLanguage() == "en" ? OrderStatus.DriverApproved.rawValue : OrderStatusArabic.DriverApproved.rawValue
        case 2:
            orderStatus.text = Languages.currentAppleLanguage() == "en" ? OrderStatus.ClientpostItems.rawValue : OrderStatusArabic.ClientpostItems.rawValue
            
        case 3:
            orderStatus.text = Languages.currentAppleLanguage() == "en" ? OrderStatus.InProcess.rawValue : OrderStatusArabic.InProcess.rawValue
            
        case 4:
            orderStatus.text = Languages.currentAppleLanguage() == "en" ? OrderStatus.WashedItemDelivering.rawValue : OrderStatusArabic.WashedItemDelivering.rawValue
            
        case 5:
            orderStatus.text = Languages.currentAppleLanguage() == "en" ? OrderStatus.ClientFinishService.rawValue : OrderStatusArabic.ClientFinishService.rawValue
        case 6:
            orderStatus.text = Languages.currentAppleLanguage() == "en" ? OrderStatus.DELIVERED.rawValue : OrderStatusArabic.DELIVERED.rawValue
            
        default:
            print ("No order")//println("Integer out of range")
        }
        

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func Cancel()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Table view data source
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return orders!.items.count+1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderDetailsItemCell", for: indexPath) as! orderDetailsItemCell
        if indexPath.row > 0
        {
        let order = orders!.items[indexPath.row-1]
        cell.order = order
        cell.SetupCell()
        }
        else
        {
        }
        return cell
    }
    
    
    
}
