//
//  SettingsVC.swift
//  Laundriya
//
//  Created by Yo7ia on 5/7/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//




import UIKit
import SideMenu

class SettingsVC: UIViewController {
    
    
    @IBOutlet weak var changelanguage: UIButton!
    @IBOutlet weak var profile: UIButton!
    @IBOutlet weak var icon: UIButton!

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.handleLocalizationChange()
        if Languages.currentAppleLanguage() == "ar"
        {
            if let _img = icon.currentImage {
                icon.setImage( UIImage(cgImage: _img.cgImage!, scale:_img.scale , orientation: UIImageOrientation.upMirrored), for: .normal)
            }
        }

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        changelanguage.Curvyimage()
        profile.Curvyimage()
        

        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func CancelClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func changeLanguage(_ sender: AnyObject) {
        if Languages.currentAppleLanguage() == "en" {
            Languages.setAppleLAnguageTo(lang: "ar")
//            Bundle.main.path(forResource: "ar" as String, ofType: "lproj")

            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            let storyboard = UIStoryboard(name: "MainAr", bundle: nil)
            AppDelegate.vc = storyboard.instantiateViewController(withIdentifier: "SideMenuRootVC") as! SideMenuRootVC
            let menuRightNavigationController = UISideMenuNavigationController(rootViewController: AppDelegate.vc)
            menuRightNavigationController.leftSide = false
            menuRightNavigationController.navigationBar.isHidden = true
            SideMenuManager.menuRightNavigationController = menuRightNavigationController
            let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainVC")
            // self.show(mainViewController, sender: self)
            let window = UIApplication.shared.windows[0] as UIWindow;
            window.rootViewController = mainViewController
            window.makeKeyAndVisible()
//            self.dismiss(animated: false, completion: nil)
            
        } else {
            Languages.setAppleLAnguageTo(lang: "en")
//            Bundle.main.path(forResource: "en" as String, ofType: "lproj")

            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            AppDelegate.vc = storyboard.instantiateViewController(withIdentifier: "SideMenuRootVC") as! SideMenuRootVC
            let menuRightNavigationController = UISideMenuNavigationController(rootViewController: AppDelegate.vc)
            menuRightNavigationController.leftSide = false
            menuRightNavigationController.navigationBar.isHidden = true
            SideMenuManager.menuRightNavigationController = menuRightNavigationController
            let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainVC")
            // self.show(mainViewController, sender: self)
            let window = UIApplication.shared.windows[0] as UIWindow;
            window.rootViewController = mainViewController
            window.makeKeyAndVisible()
            
//            self.dismiss(animated: false, completion: nil)
            //self.performSegue(withIdentifier: "showCategory", sender: self)
            
        }
        
    }
    
}
