//
//  FirstViewNavigationController.swift
//  Washer-Client
//
//  Created by Yo7ia on 4/10/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import Foundation
import UIKit


class FirstViewNavigationController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        checkLogin()
    }
    fileprivate func checkLogin() {
        var  storyboard : UIStoryboard?
        
        storyboard = Languages.currentAppleLanguage() == "en" ? UIStoryboard(name: "Main", bundle: nil) : UIStoryboard(name: "MainAr", bundle: nil)
        if let _ = UserDefaults.standard.object(forKey: "AccessToken") {
            let userName = UserDefaults.standard.object(forKey: "userName") as! String
            let one = 1
            let mobilenumber = "\(one)\(userName)"
            //  let password = UserDefaults.standard.object(forKey: "password") as! String
            let code = UserDefaults.standard.object(forKey: "Code") as! String
            ProgressUtility.showProgressView()

            Networking.Confirm(code: code, mobileNumber: userName) { (Code) in
                
            }
//
            Networking.login(mobileNumber: userName, password: code) { (code) in
                ProgressUtility.dismissProgressView()

                if code.code == 3 {
                    
                    //let alrt = SCLAlertView()
                    //alrt.showTitle("Yes!", subTitle: "Complte registrtion", style: .success,colorStyle: 0x32CD32)
                    print("AAAAAAAAAAAAAAAA",code.AccessToken!)
                    UserDefaults.standard.set(code.AccessToken, forKey: "AccessToken")
                    UserDefaults.standard.synchronize()
                    
                    
                    let window = UIApplication.shared.delegate!.window
                    let mainViewController = storyboard!.instantiateViewController(withIdentifier: "MainVC")
                    window!?.rootViewController = mainViewController
                    window!?.makeKeyAndVisible()
                    
                    
                }else {
//                    UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
//                    UserDefaults.standard.synchronize()
//                    let window = UIApplication.shared.delegate!.window
//                    let mainViewController = storyboard!.instantiateViewController(withIdentifier: "MainVC")
//                    window!?.rootViewController = mainViewController
//                    window!?.makeKeyAndVisible()
                    //  let alrt = SCLAlertView()
                    //  alrt.showError("Code Check", subTitle: "Somthing et worng")
                    
                }
            }
            
            //            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            //
            //            let mainViewController = storyboard.instantiateViewController(withIdentifier: "Main")
            //            self.window?.rootViewController = mainViewController
            //            self.window?.makeKeyAndVisible()
            //            locationManager.requestWhenInUseAuthorization()
        }
        else
        {
            let window = UIApplication.shared.delegate!.window
            let mainViewController = storyboard!.instantiateViewController(withIdentifier: "FirstView")
            window!?.rootViewController = mainViewController
            window!?.makeKeyAndVisible()
        }
    }

}
