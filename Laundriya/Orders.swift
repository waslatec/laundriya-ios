//
//  Orders.swift
//  Washer-Client
//
//  Created by Amr on 2/7/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import ObjectMapper


class Orders: Mappable {
    var Orders: [Order]?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        Orders <- map["OrdersList"]
      
    }
}
class Order: Mappable {
    
    var Id = 0
    var Date = ""
    var Cost = 0.0
    // var imagePath: String?
    var State = 0
    var items = [Item]()
    var Category = [CategoryDTO]()

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Id <- map["Id"]
        Date <- map["Date"]
        Cost <- map["Cost"]
        State <- map["State"]
        items <- map["items"]
        Category <- map["Category"]
    }
    
}
class Item: Mappable {
    
    var id: Int?
    var englishName: String?
    var arabicName: String?
    // var imagePath: String?
    var amount: Int?
    var unitePrice: Int?
    var unitpoint: Int?
    var notes: String?
    var serviceEnglishName: String?
    var serviceArabicName: String?
    var serviceTypeArabicName: String?
    var serviceTypeEnglishName: String?



    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["Id"]
        englishName <- map["EnglishName"]
        arabicName <- map["ArabicName"]
       amount <- map["Amount"]
        unitpoint <- map["unitpoint"]
        notes <- map["Notes"]
        serviceEnglishName <- map["ServiceEnglishName"]
        serviceArabicName <- map["ServiceArabicName"]
        serviceTypeArabicName <- map["ServiceTypeArabicName"]
        serviceTypeEnglishName <- map["ServiceTypeEnglishName"]
        unitePrice <- map["unitePrice"]
 
    }


}
