//
//  MapVC.swift
//  Washer-Client
//
//  Created by Amr on 1/29/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import UIKit
import GoogleMaps
import Foundation
import ObjectMapper
import Alamofire

class MapVC: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {
    
    
    @IBOutlet weak var mapMarkerImage: UIImageView!
    @IBOutlet var viewMap: GMSMapView!
    var marker : GMSMarker?
    var secondmarker : GMSMarker?
    var testgecoding = MapTasks()
    var userAddress: ((lookupAddress) -> ())?
    var prevLocation : CLLocation?
    
    var locationManager = CLLocationManager()
    var didFindMyLocation = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()

        if CLLocationManager.locationServicesEnabled()
            && CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse {
        locationManager.startUpdatingLocation()
            locationManager.delegate = self
            viewMap.delegate = self
            viewMap.settings.myLocationButton = false
           
        }
        }
  
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.first
        if let location = location {
            
            if prevLocation == nil {
                prevLocation = location
                getLocation(location: prevLocation!)
            } else {
                if prevLocation!.distance(from: location) > 500 {
                    prevLocation = location
                    getLocation(location: prevLocation!)
                }
            }
        }
    }
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        getLocation(location: location)
    }
//    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
//        if prevLocation != nil {
//            getLocation(location: prevLocation!)
//        }
//    }
    
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        if prevLocation != nil {
            marker?.map = nil
        getLocation(location: prevLocation!)
        }
        return true
    }
    func getLocation(location: CLLocation) {
        
        viewMap.camera = GMSCameraPosition.camera(withTarget: location.coordinate, zoom: 16.0)
        
//        marker?.map = nil
//        marker = GMSMarker(position: location.coordinate)
//        marker?.icon = UIImage(named: "mapMarkerUser")
//        marker?.map = viewMap
        
        
//        let address = lookupAddress()
//        address.lat = location.coordinate.latitude
//        address.lng = location.coordinate.longitude
//        userAddress?(address)
    }
    
    func drawMarker(location: CLLocation,title: String) {
                marker?.map = nil
                marker = GMSMarker(position: location.coordinate)
                marker?.icon = UIImage(named: "driverPin")
                marker?.map = viewMap
                marker?.title = title
    }
    func drawMarkerWithImage(location: CLLocation,title: String,image: UIImage) {
        marker?.map = nil
        marker = GMSMarker(position: location.coordinate)
        secondmarker?.icon = image.resizeImage(newWidth: 60)
        
        marker?.map = viewMap
        marker?.title = title
    }
    func drawSecondMarkerWithImage(location: CLLocation,title: String,image: UIImage) {
        secondmarker?.map = nil
        secondmarker = GMSMarker(position: location.coordinate)
        secondmarker?.icon = image.resizeImage(newWidth: 50)
        
        secondmarker?.map = viewMap
        secondmarker?.title = title
    }
    func ClearMap() {
        viewMap.clear()
    }
    
//    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
//      //  getLocation(location: prevLocation!)
//        var des = CLLocation()
//        if viewMap.settings.scrollGestures == true {
//            des = CLLocation(latitude: position.target.latitude, longitude: position.target.longitude)
//            getLocation(location: des)
//        }
//    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
//        var des = CLLocation()
//        if viewMap.settings.scrollGestures == true {
//            des = CLLocation(latitude: position.target.latitude, longitude: position.target.longitude)
//            getLocation(location: des)
//        }
        
        let coor = mapView.projection.coordinate(for: viewMap.center)
        let address = lookupAddress()
        address.lat = coor.latitude
        address.lng = coor.longitude
        userAddress?(address)
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "maporderNow"{
            
//            let mapNo = segue.destination as! OrderNowVC
//            mapNo.userLocation = { (address) in
//                
//                MapTasks.geocodeAddress(address: address.formateer!, completion: { (lookupAddressResults) in
//                    print("LLLLLLLLLLLLLLLLLL",lookupAddressResults.resultsAdd?.first?.lat)
//                       print(lookupAddressResults.resultsAdd?.first?.lat)
//                })
//                
//            }
            
        }
       
        
    
 

}
    
    func getPolylineRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D){
       
            let url = URL(string: "http://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=false&mode=driving")!
            let accessToken = UserDefaults.standard.object(forKey: "AccessToken") as! String
            let headers: HTTPHeaders = ["Authorization": "Bearer \(accessToken) "]
            print(accessToken)
            Alamofire.request( url, method: .post , headers: headers).responseString { response in
                if let JSON = response.result.value {
                    if JSON != "null" {
                     let r = Mapper<dic>().map(JSONString: JSON)!
                        if (r.resultsAdd?.count)! > 0
                        {
                         self.showPath(polyStr: (r.resultsAdd?[0].point!)!)
                        }
                    }else{
                        
                    }
                } else {
                    print("Error")
                }
            }
       
    }
    
    func showPath(polyStr :String){
        let path = GMSPath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 8
        polyline.strokeColor = UIColor(fromARGBHexString: "01B0F0")
        polyline.map = viewMap // Your map view
    }



}


class dic: Mappable {
    var resultsAdd : [ddc]?
    
    init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        resultsAdd <- map["routes"]
        
    }
    
}
class ddc: Mappable {
    
    var point : String?
    
    
    init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        point <- map["overview_polyline.points"]
        
        
        
    }
}


