//
//  DimUtility.swift
//  Le Cadeau
//
//  Created by Mohammad Shaker on 2/17/16.
//  Copyright © 2016 AMIT-Software. All rights reserved.
//

import UIKit

class DimUtility {
    
    fileprivate static var dimView: UIView! = UIView()
    
    class func setDimViewStyles() {
        dimView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        dimView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
    }
    
    class func addDimView() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController!.view.addSubview(dimView)
    }
    
    class func removeDimView() {
        dimView.removeFromSuperview()
    }
    
}
