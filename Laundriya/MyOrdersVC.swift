//
//  MyOrdersVC.swift
//  Washer-Client
//
//  Created by Amr on 2/7/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import UIKit
import CCMPopup

class MyOrdersVC: UIViewController,UITableViewDelegate,UITableViewDataSource,ItemsCellDelagete {
    var orders = [Order]()

    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var icon: UIButton!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.handleLocalizationChange()
        if Languages.currentAppleLanguage() == "ar"
        {
            if let _img = icon.currentImage {
                icon.setImage( UIImage(cgImage: _img.cgImage!, scale:_img.scale , orientation: UIImageOrientation.upMirrored), for: .normal)
            }
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.handleLocalizationChange()
        self.hideKeyboardWhenTappedAround()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        ProgressUtility.showProgressView()

        Networking.GetOrders { (Orders) in
            ProgressUtility.dismissProgressView()

            self.orders = Orders.Orders ?? []
            self.orders.sort(by: { (o, p) -> Bool in
                return o.Date.getDateFromString().compare(p.Date.getDateFromString()) == ComparisonResult.orderedDescending
            })
            self.tableView?.reloadData()
        }
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func Cancel()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Table view data source

    
     func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return orders.count
    }

    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemsCell", for: indexPath) as! ItemsCell
        
        let order = orders[indexPath.row]
        cell.order = order
        cell.delegate = self

        cell.OrderItems.text = ""
        for i in order.Category
        {
            let te = Languages.currentAppleLanguage() == "ar" ? i.arabicName : i.englishName
            if (order.Category.count) > 1
            {
                cell.OrderItems.text = cell.OrderItems.text! + " , " + te!
                
            }
            else
            {
                cell.OrderItems.text = cell.OrderItems.text! + "" + te!
                
            }
        }
        
        cell.OrderDates.text = order.Date.replacingOccurrences(of: "T", with: " ")
        var Cost = 0
        for i in order.items
        {
            Cost += i.amount! * i.unitePrice!
        }
        let curr = Languages.currentAppleLanguage() == "ar" ? "ريال " : " SAR"
        cell.OrderPrice.text = "\(Cost) "+curr

        switch (order.State)
        {
        case 0:
            cell.OrderStatus.text = Languages.currentAppleLanguage() == "en" ? OrderStatus.Requested.rawValue : OrderStatusArabic.Requested.rawValue
            
        case 1:
            cell.OrderStatus.text = Languages.currentAppleLanguage() == "en" ? OrderStatus.DriverApproved.rawValue : OrderStatusArabic.DriverApproved.rawValue
        case 2:
            cell.OrderStatus.text = Languages.currentAppleLanguage() == "en" ? OrderStatus.ClientpostItems.rawValue : OrderStatusArabic.ClientpostItems.rawValue
            
        case 3:
            cell.OrderStatus.text = Languages.currentAppleLanguage() == "en" ? OrderStatus.InProcess.rawValue : OrderStatusArabic.InProcess.rawValue
            
        case 4:
            cell.OrderStatus.text = Languages.currentAppleLanguage() == "en" ? OrderStatus.WashedItemDelivering.rawValue : OrderStatusArabic.WashedItemDelivering.rawValue
            
        case 5:
            cell.OrderStatus.text = Languages.currentAppleLanguage() == "en" ? OrderStatus.ClientFinishService.rawValue : OrderStatusArabic.ClientFinishService.rawValue
        case 6:
            cell.OrderStatus.text = Languages.currentAppleLanguage() == "en" ? OrderStatus.DELIVERED.rawValue : OrderStatusArabic.DELIVERED.rawValue
            
        default:
            print ("No order")//println("Integer out of range")
        }
        
        return cell
    }
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//       self.performSegue(withIdentifier: "orderDetailsVC", sender: tableView.cellForRow(at: indexPath))
    }
 



    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "orderDetailsVC" {
        let vc = segue.destination as! OrderDetailsVC
            vc.orders = (sender as! ItemsCell).order
        }
        
    }
    func ItemsCellSelected(_ cell: ItemsCell) {
        performSegue(withIdentifier: "orderDetailsVC", sender: cell)
    }
   

}
