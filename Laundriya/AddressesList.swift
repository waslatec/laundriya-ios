//
//  AddressesList.swift
//  Washer-Client
//
//  Created by Amr on 2/13/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import Foundation
import ObjectMapper


class  AddressesList: Mappable {
    var address: [Address]?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        address <- map["Addresses"]
        
    }
}

class Address: Mappable {
    
    var id: Int?
    var addressHint: String?
    var clientId: Int?
    var deliveryAddress: String?
    var flatNumber: Int?
    var floorNumber: Int?
    var lat : Double?
    var lng : Double?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["Id"]
        addressHint <- map["AddressHint"]
        clientId <- map["ClientId"]
        deliveryAddress <- map["DeliveryAddress"]
        flatNumber <- map["FlatNumber"]
        floorNumber <- map["FloorNumber"]
        lat <- map["Latitude"]
        lng <- map["Logitude"]
        
        
    }

}
