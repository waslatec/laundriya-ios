//
//  UserProfile.swift
//  Washer-Client
//
//  Created by Amr on 1/23/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import ObjectMapper

class UserProfile: Mappable {
    
    
    var UserType: Int?
    var ActivationCode: String?
    var UserName: String?
    var PhoneNumber: String?
    var Email: String?
    var Description: String?
    var MapLat: Double?
    var MapLng: Double?
    var PicturePath: String?
    var ShowPhone: Bool?
    var Activated: Bool?
    var Blocked: Bool?
    var Password: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        UserType <- map["UserType"]
        ActivationCode <- map["ActivationCode"]
        UserName <- map["Name"]
        PhoneNumber <- map["PhoneNumber"]
        Email <- map["Email"]
        Description <- map["Description"]
        MapLat <- map["MapLat"]
        MapLng <- map["MapLng"]
        PicturePath <- map["PicturePath"]
        ShowPhone <- map["ShowPhone"]
        Activated <- map["Activated"]
        Blocked <- map["Blocked"]
        Password <- map["Password"]
    }
    
}


class AccessToken : Mappable {
    
//    var AccessToken: String? {
//        didSet {
//            UserDefaults.standard.set(AccessToken, forKey: "AccessToken")
//            UserDefaults.standard.synchronize()
//        }
//    }
    var userName : String?{
        didSet {
            UserDefaults.standard.set(userName, forKey: "userName")
            UserDefaults.standard.synchronize()
        }
    }
    var error : String?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        
       // AccessToken <- map["access_token"]
        userName <- map["client.username"]
        error <- map["error"]
    }
}
