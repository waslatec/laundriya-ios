//
//  MyOrdersVC.swift
//  Washer-Client
//
//  Created by Amr on 2/7/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import UIKit
import CCMPopup

class OrdersVC: UITableViewController {
    var orders = [Item]()
    var orderss = [Order]()
    
    @IBOutlet weak var tatalCost: UILabel!
    @IBOutlet weak var totalCostBE: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Languages.currentAppleLanguage() == "ar" {
            tatalCost.text = "المجموع"
        }
        //      tableView.contentOffset = CGPoint(x: 0, y: -64)
        self.hideKeyboardWhenTappedAround()
        
        Networking.GetOrders { (Orders) in
            print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%",Orders.Orders?[0].MyItems)
            self.orders = Orders.Orders?[0].MyItems ?? []
            self.orderss = Orders.Orders ?? []
            let cost = self.orderss.first?.cost!
            self.totalCostBE.text = "\(cost!)"
            self.tableView?.reloadData()
        }
        // self.navigationController?.navigationBar.isTranslucent = false
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.backgroundColor = UIColor.clear
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return orders.count
        
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath) as! OrderCell
        
        let order = orders[indexPath.row]
        
        if Languages.currentAppleLanguage() == "en" {
        cell.itemName.text = order.englishName
        cell.price.text = String(describing: order.unitePrice!)
        cell.amount.text = String(describing: order.amount!)
        cell.serviceType.text = order.serviceTypeEnglishName
        }else {
            cell.itemName.text = order.arabicName
            cell.price.text = String(describing: order.unitePrice!)
            cell.amount.text = String(describing: order.amount!)
            cell.serviceType.text = order.serviceTypeArabicName
        }
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            // self.performSegue(withIdentifier: "orderd", sender: Any?.self)
        }
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let indexPath = tableView?.indexPath(for: sender as! OrderCell)
        
        let cost = orderss[indexPath!.row].cost
       let oo = segue.destination as! OrderDetailsVC
       oo.totalcost = cost!
        
    }
    
    
}
