//
//  Calendar.swift
//  Washer-Client
//
//  Created by Mohamed Namir on 3/26/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import ObjectMapper


class Calendarr: Mappable {
    var Data: [Data]?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        Data <- map["Data"]
        
    }
}

class Data: Mappable {
    
    var day: Int?
    var times : [String]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        day <- map["Day"]
        times <- map["Times"]
        
    }
    
}
