//
//  User.swift
//  Washer-Client
//
//  Created by Amr on 1/23/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import Foundation

import ObjectMapper

class User: Mappable {
    var userData: UserProfile?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        userData <- map["Data"]
    }
}

class Code: Mappable {
    var code : Int?
    var AccessToken: String? 
    var userName : String?
    var password : String?
    var mobileNumber : String?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        code <- map["Code"]
        AccessToken <- map["Token"]
        userName <- map["client.username"]
         password <- map["client.password"]
        mobileNumber <- map["client.MobileNumber"]

        
    }
}
