//
//  Categories.swift
//  Washer-Client
//
//  Created by Amr on 2/1/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import ObjectMapper


class Categories: Mappable {
    var Categories: [CategoryDTO]?
    var imagePath: String?
    var services: [Service]?
    var servicestypes: [ServiceType]?
    var prices: [Prices]?
    var Items : [CategoryDTO]?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        Categories <- map["categories"]
        Items <- map["Items"]

        services <- map["services"]
        servicestypes <- map["servicestypes"]
        prices <- map["prices"]
        imagePath <- map["CategoryImgURL"]
    }
}
class CategoryDTO: Mappable {
    
    var id: Int?
    var englishName: String?
    var arabicName: String?
   // var imagePath: String?
    var englishDescription: String?
    var arabicDescription: String?
    var category: Int?
    var price: Int?
    var unitpoint: Int?
    var estimatedCockingTime: Date?
    var min: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["Id"]
        englishName <- map["EnglishName"]
        arabicName <- map["ArabicName"]
//        englishDescription <- map["EnglishDescription"]
//        arabicDescription <- map["ArabicDescription"]
//        category <- map["Category"]
//        price <- map["Price"]
//        unitpoint <- map["unitpoint"]
//        estimatedCockingTime <- map["EstimatedCockingTime"]
        min <- map["min"]
    }
    
}
class Service: Mappable {
    
    var Id: Int?
    var EnglishName: String?
    var ArabicName: String?
       required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Id <- map["Id"]
        EnglishName <- map["EnglishName"]
        ArabicName <- map["ArabicName"]

    }
    
}
class ServiceType: Mappable {
    
    var Id: Int?
    var EnglishName: String?
    var ArabicName: String?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Id <- map["Id"]
        EnglishName <- map["EnglishName"]
        ArabicName <- map["ArabicName"]

    }
    
}
class Prices: Mappable {
    
    var Price: Int?
    var Item: Int?
    var serviceId: Int?
    var serviceTypeId: Int?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Price <- map["Price"]
        Item <- map["Item"]
        serviceId <- map["serviceId"]
        serviceTypeId <- map["serviceTypeId"]
        
    }
    
}
class PriceListModel: Mappable {
    
    var Price = 0
    var ItemId = 0
    var ItemArabicName = ""
    var ItemEnglishName = ""
    var ServiceArabicName = ""
    var ServiceEnglishName = ""
    var ServiceId = 0
    var ServiceTypeArabicName = ""
    var ServiceTypeEnglishName = ""
    var serviceTypeId = 0
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        
        
    }
    
}



