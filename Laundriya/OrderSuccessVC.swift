//
//  OrderSuccessVC.swift
//  Laundriya
//
//  Created by Yo7ia on 5/4/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//

import Foundation

import UIKit
import CCMPopup
import SideMenu

class  OrderSuccessVC: UIViewController {
    
    var successfullCompletionHandler: ((Void) -> Void)?
    var failureCompletionHandler: ((Void) -> Void)?
    var isOrderNow = true

    @IBOutlet weak var mincharge: UILabel!
    @IBOutlet weak var goToHome: UIView!
    @IBOutlet weak var TrackOrder: UIView!
    @IBOutlet weak var DismissPopUp: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        if isOrderNow
        {
            goToHome.isHidden = false
            TrackOrder.isHidden = false
            DismissPopUp.isHidden = true
            mincharge.text = Languages.currentAppleLanguage() == "en" ? "The driver is in his way" : "السائق في الطريق"
        }
        else
        {
            goToHome.isHidden = true
            TrackOrder.isHidden = true
            DismissPopUp.isHidden = false
            mincharge.text = Languages.currentAppleLanguage() == "en" ? "Your order has been placed in\nthe queue successfully." : "تم الطلب بنجاح"
        }
        
        
    }
    @IBAction func TrackOrderClicked(){
       
            self.successfullCompletionHandler!()
       
    }
    @IBAction func gotoHomeClicked(){
        
        self.failureCompletionHandler!()
        
        
    }
    
    @IBAction func Cancel()
    {
        self.failureCompletionHandler!()
    }
    
    
}
