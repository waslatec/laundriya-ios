//
//  ConfirmVC.swift
//  Washer-Client
//
//  Created by Amr on 1/31/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import UIKit
import PinCodeView
class ConfirmVC: UIViewController ,PinCodeViewDelegate  {
    @IBOutlet weak var requestCodeBut: UIButton!
    @IBOutlet weak var verifyBut: UIButton!
    @IBOutlet weak var pinView: PinCodeView! {
        didSet {
            pinView.delegate = self
            pinView.numberOfDigits = 6
            pinView.groupingSize = 0
            pinView.itemSpacing = 7
            pinView.digitViewInit = PinCodeDigitSquareView.init
        }
    }
    var userName = ""
    var Code = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        

        style()
    }
    func pinCodeView(_ view: PinCodeView, didSubmitPinCode code: String, isValidCallback callback: @escaping (Bool) -> Void) {
        Code = code
        ConfirmCode(code: code)
        // here you can show your progress or do any UI action
        
        }
    
    func pinCodeView(_ view: PinCodeView, didInsertText text: String) {
        // that's the place for hiding error views, etc
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    @IBAction func getRequest(_ sender: AnyObject) {
        ProgressUtility.showProgressView()

        Networking.RequestCode(mobileNumber: userName) { (Code) in
            ProgressUtility.dismissProgressView()

            if Code.code == 3 {
                
                if Languages.currentAppleLanguage() == "ar" {
                _ = SweetAlert().showAlert(NSLocalizedString("الكود", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("تم ارسال الكود بنجاح", comment: "Localize me Label in the ConfirmVC scene"), style: .success)
                }else {
                    _ = SweetAlert().showAlert(NSLocalizedString("Code", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("The code send successfuly", comment: "Localize me Label in the ConfirmVC scene"), style: .success)
                }

            }else {
                if Languages.currentAppleLanguage() == "ar" {
                    _ = SweetAlert().showAlert(NSLocalizedString("الكود", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("حدث خطا اعد المحاوله", comment: "Localize me Label in the ConfirmVC scene"), style: .error)
                }else {
                    _ = SweetAlert().showAlert(NSLocalizedString("Code Check", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("Somthing went worng", comment: "Localize me Label in the ConfirmVC scene"), style: .error)
                }
            
            }

        }
    }
    func ConfirmCode(code: String)
    {
        self.dismissKeyboard()

       // SwiftOverlays.showBlockingWaitOverlay()
        let code = code
        let one = 1
        let mobilenumber = "\(one)\(userName)"
        UserDefaults.standard.set(code, forKey: "Code")
        UserDefaults.standard.synchronize()
        Networking.Confirm(code: code, mobileNumber: userName) { (Code) in
            
        }
        ProgressUtility.showProgressView()

        Networking.login(mobileNumber: mobilenumber, password: code) { (code) in
            ProgressUtility.dismissProgressView()

            if code.code == 3 {
                //let alrt = SCLAlertView()
                //alrt.showTitle("Yes!", subTitle: "Complte registrtion", style: .success,colorStyle: 0x32CD32)
                
                UserDefaults.standard.set(code.AccessToken, forKey: "AccessToken")
                UserDefaults.standard.synchronize()
                UserDefaults.standard.set(code.userName, forKey: "userName")
                UserDefaults.standard.synchronize()
                UserDefaults.standard.set(code.mobileNumber, forKey: "mobileNumber")
                UserDefaults.standard.synchronize()
                print("Success Login")
                self.performSegue(withIdentifier: "GoToHome", sender: Any?.self)
            }else if code.code == 2 {
                _ = SweetAlert().showAlert(NSLocalizedString("Code Check", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("Invaild code", comment: "Localize me Label in the ConfirmVC scene"), style: .error)
                self.pinView.digitViewInit = PinCodeDigitSquareView.init
                self.pinView.reloadInputViews()
                //                let alrt = SCLAlertView()
                //                alrt.showError("Code Check", subTitle: "Invaild code")
            }else {
                _ = SweetAlert().showAlert(NSLocalizedString("Code Check", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("Somthing went worng", comment: "Localize me Label in the ConfirmVC scene"), style: .error)
                self.pinView.digitViewInit = PinCodeDigitSquareView.init
                self.pinView.reloadInputViews()
                //                let alrt = SCLAlertView()
                //                alrt.showError("Code Check", subTitle: "Somthing et worng")
                
            }
        }

    }
    @IBAction func confirm(_ sender: AnyObject) {
        self.dismissKeyboard()

        if Code != ""
        {
            ConfirmCode(code: Code)
        }
        else
        {
                SweetAlert().showAlert("Note!", subTitle: "Enter activation code ", style: AlertStyle.warning)
                return
        
        }
        
        }
    @IBAction func Cancel()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func style() {
        
        self.hideKeyboardWhenTappedAround()
        self.view.layer.cornerRadius = 8
        requestCodeBut.layer.cornerRadius = 5
        verifyBut.layer.cornerRadius = 5
        
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if let text = textField.text {
//            if(text.characters.count == 6)  {
//                self.showWaitOverlay()
//                textField.isEnabled = false
//                let code = codeText.text
//                Networking.Confirm(code: code!, mobileNumber: userName) { (code) in
//                    if code.code == 3 {
//                        let alrt = SCLAlertView()
//                        alrt.showTitle("Yes!", subTitle: "Complte registrtion", style: .success,colorStyle: 0x32CD32)
//                        SwiftOverlays.removeAllBlockingOverlays()
//                        textField.isEnabled = true
//                    }else if code.code == 4 {
//                        SwiftOverlays.removeAllBlockingOverlays()
//                        let alrt = SCLAlertView()
//                        alrt.showError("Code Check", subTitle: "Invaild code")
//                         textField.isEnabled = true
//                        textField.text = ""
//                    }else {
//                        let alrt = SCLAlertView()
//                        alrt.showError("Code Checkkkk", subTitle: "Somthing et worng")
//                        SwiftOverlays.removeAllBlockingOverlays()
//                         textField.isEnabled = true
//                    }
//                }
//            }
//            else {
//                
//                textField.isEnabled = true
//             }
//        }
//        textField.isEnabled = true
//        return true
//    }
    
}
