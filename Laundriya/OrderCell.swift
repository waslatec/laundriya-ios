//
//  OrderCell.swift
//  Washer-Client
//
//  Created by Amr on 1/29/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell {
    
    
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var serviceType: UILabel!
    @IBOutlet weak var LitemName: UILabel!
    @IBOutlet weak var LAmount: UILabel!
    @IBOutlet weak var LPrice: UILabel!
    
    @IBOutlet weak var LSreviceType: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if Languages.currentAppleLanguage() == "ar"{
            
            LitemName.text = "أسم الصنف"
            LAmount.text = "الكميه"
            LPrice.text = "السعر"
            LSreviceType.text = "نوع الخدمه"
            
        }
    }

}
