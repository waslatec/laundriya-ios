//
//  FinishServiceVC.swift
//  Laundriya
//
//  Created by Yo7ia on 5/14/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//

import Foundation
//
import UIKit
import CCMPopup
import SideMenu

class FinishServiceVC: UIViewController {
    
    var orderID = -1
    var successfullCompletionHandler: ((Int) -> Void)?
    
    @IBOutlet weak var mincharge: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        mincharge.text = Languages.currentAppleLanguage() == "en" ? "Order No. : " : "رقم الطلب : "
        mincharge.text = mincharge.text!+"\n"+Languages.currentAppleLanguage() == "en" ? "Your Order Has Been Delivered" : "تم تسليم المنتجات"
        
        
    }
    @IBAction func Proceed()
    {
        self.successfullCompletionHandler?(orderID)
        

    }
    
}
