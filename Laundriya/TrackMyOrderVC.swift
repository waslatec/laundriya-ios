//
//  TrackMyOrderVC.swift
//  Laundriya
//
//  Created by Yo7ia on 5/4/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//


import UIKit
import SideMenu
import Kingfisher
import CoreLocation
import FlexibleSteppedProgressBar
class TrackMyOrderVC: UIViewController, FlexibleSteppedProgressBarDelegate{
    
    // var menuButton = UIBarButtonItem()
    var maxIndex = -1
    var mapVc : MapVC?
    var userCurrentLocation : CLLocation?
    @IBOutlet weak var progressBarWithoutLastState : FlexibleSteppedProgressBar!
    @IBOutlet weak var orderNumberLabel : UILabel!
    var currentOrder :Int?
    var order : Order?
    var check = false
    @IBOutlet weak var icon: UIButton!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if Languages.currentAppleLanguage() == "ar"
        {
            if let _img = icon.currentImage {
                icon.setImage( UIImage(cgImage: _img.cgImage!, scale:_img.scale , orientation: UIImageOrientation.upMirrored), for: .normal)
            }
        }
        
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.handleLocalizationChange()

        HubConnection.startConnection()
        self.hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(TrackMyOrderVC.DriverUpdated(notification:)), name: Notification.Name("driverUpdate"), object: nil)
            setupFlexibleBar()
        orderNumberLabel.text = String(describing: currentOrder!)
       
        if order != nil
        {
            UpdateUI(orderID: order!.Id, status: order!.State, lat: -1 , lng: -1)
        }

    }
    
    func setupFlexibleBar()
    {
        progressBarWithoutLastState.numberOfPoints = 4
        
        progressBarWithoutLastState.delegate = self
        progressBarWithoutLastState.selectedOuterCircleStrokeColor = UIColor(fromARGBHexString: "01B0F0")
        progressBarWithoutLastState.stepTextColor = UIColor.clear
        progressBarWithoutLastState.currentSelectedTextColor = UIColor.clear
        progressBarWithoutLastState.backgroundShapeColor = UIColor.white
        progressBarWithoutLastState.lastStateOuterCircleStrokeColor = UIColor(fromARGBHexString: "01B0F0")
        progressBarWithoutLastState.selectedBackgoundColor = UIColor(fromARGBHexString: "01B0F0")
        progressBarWithoutLastState.selectedOuterCircleStrokeColor = UIColor(fromARGBHexString: "01B0F0")
        progressBarWithoutLastState.currentSelectedCenterColor = UIColor(fromARGBHexString: "01B0F0")
        
        progressBarWithoutLastState.currentIndex = Languages.currentAppleLanguage() == "en" ? 0 : 0
    }
    
    func DriverUpdated(notification: Notification){
        if let orderId = notification.userInfo?["orderId"] as? Int {
            if orderId == currentOrder
            {
                let status = notification.userInfo?["status"] as? Int
                let lat = notification.userInfo?["lat"] as? Double
                let lng = notification.userInfo?["lng"] as? Double
                UpdateUI(orderID: orderId, status: status!, lat: lat!, lng: lng!)
                
            }
            print(orderId)
        }else{
            orderNumberLabel.text = "No Order Tracking"
        }
//
        
    }
    
    func UpdateUI(orderID: Int , status: Int , lat: Double , lng : Double)
    {
        currentOrder = orderID
        switch (status)
        {
        case 0:
            progressBarWithoutLastState.currentIndex = Languages.currentAppleLanguage() == "en" ? 0 : 0
            
        case 1:
            progressBarWithoutLastState.currentIndex = Languages.currentAppleLanguage() == "en" ? 0 : 0
            
            
        case 2:
            progressBarWithoutLastState.currentIndex = Languages.currentAppleLanguage() == "en" ? 1 : 1
            
            
        case 3:
            progressBarWithoutLastState.currentIndex = Languages.currentAppleLanguage() == "en" ? 1 : 1
            
        case 4:
            progressBarWithoutLastState.currentIndex = Languages.currentAppleLanguage() == "en" ? 2 : 2
            
            
        case 5:
            progressBarWithoutLastState.currentIndex = Languages.currentAppleLanguage() == "en" ? 3 : 3
        case 6:
            progressBarWithoutLastState.currentIndex = Languages.currentAppleLanguage() == "en" ? 3 : 3
            
        default:
            progressBarWithoutLastState.currentIndex = Languages.currentAppleLanguage() == "en" ? 0 : 0
        }
//        let lat = notification.userInfo?["lat"] as? Double
//        let lng = notification.userInfo?["lng"] as? Double
        print("######################################################",lat)
        
        if lat != -1 && lng != -1
        {
            let driverloc = CLLocation(latitude: lat, longitude: lng)
            self.mapVc!.ClearMap()
            self.mapVc!.drawMarkerWithImage(location: self.userCurrentLocation!, title: "User Location", image: #imageLiteral(resourceName: "laundriya-icons_76"))
             self.mapVc!.drawSecondMarkerWithImage(location: driverloc, title: "Order ID : "+String(describing: orderID), image: #imageLiteral(resourceName: "laundriya-icons_178"))
            self.mapVc!.getPolylineRoute(from: (self.userCurrentLocation!.coordinate), to: driverloc.coordinate)
        }
       
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       if segue.identifier == "mapVC"{
        
            mapVc = (segue.destination as! MapVC)
            mapVc!.userAddress = { (address) in
                self.mapVc!.mapMarkerImage.isHidden = true

                if address.lng != nil && address.lat != nil
                {
                    if self.userCurrentLocation == nil
                    {
                    self.userCurrentLocation = CLLocation(latitude: address.lat!, longitude: address.lng!)
                    }
                }
            }
        }
          }
    @IBAction func CancelClicked()
    {
        if check
        {
            self.navigationController?.popToRootViewController(animated: true)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBAction func ClientFinishService(_ sender: AnyObject) {
        do {
            
            try HubConnection.washHub.invoke("ClientFinishService", arguments: [currentOrder!]){ (result, error) in
                
                if let e = error {
                    print("Error: \(e)")
                } else {
                    print("Success!------------------ClientFinishService")
                    self.navigationController?.popToRootViewController(animated: true)
                    _ = SweetAlert().showAlert(Languages.currentAppleLanguage() == "ar" ? "تم بنجاح" : "Success" , subTitle: Languages.currentAppleLanguage() == "إنهاء الخدمة" ? "تم بنجاح" : "Service Finished", style: .success)
                    //  NotificationCenter.default.addObserver(self, selector: #selector(OrderNowVC.methodOfReceivedNotification(notification:)), name: Notification.Name("PendingOrder"), object: nil)
                    
                    if let r = result {
                        print("Result: \(r)")
                    }
                }
            }
        } catch {
            print(error)
        }
    }

    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     didSelectItemAtIndex index: Int) {
        progressBar.currentIndex = index
        if index > maxIndex {
            maxIndex = index
            progressBar.completedTillIndex = maxIndex
        }
    }
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     canSelectItemAtIndex index: Int) -> Bool {
        return true
    }
    
    func progressBar(_ progressBar: FlexibleSteppedProgressBar,
                     textAtIndex index: Int, position: FlexibleSteppedProgressBarTextLocation) -> String {
        
        return ""
    }
}
