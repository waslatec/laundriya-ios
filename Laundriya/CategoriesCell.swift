//
//  CategoriesCell.swift
//  Washer-Client
//
//  Created by Amr on 2/1/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import UIKit

protocol CategoriesCellDelagete {
    func CategorySelected(_ cell: CategoriesCell)
}
class CategoriesCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var categoryBackImage: UIImageView!
    var delegate : CategoriesCellDelagete?
    var category : CategoryDTO?
    var index : IndexPath?
    @IBAction func CellClicked()
    {
        self.delegate!.CategorySelected(self)
    }
    func configureCell(isSelected: Bool)
    {
        if isSelected
        {
            categoryBackImage.image = #imageLiteral(resourceName: "laundriya-icons_68")
            categoryName.textColor = UIColor(fromARGBHexString: "01B0F0")
            if categoryImage.image != nil
            {
            categoryImage.image = categoryImage.image!.maskWithColor(color: UIColor(fromARGBHexString: "01B0F0"))
            }
            
        }
        else
        {
            categoryBackImage.image = #imageLiteral(resourceName: "laundriya-icons_70")
            categoryName.textColor = UIColor(fromARGBHexString: "FFFFFF")
            if categoryImage.image != nil
            {
           categoryImage.image = categoryImage.image!.maskWithColor(color: UIColor(fromARGBHexString: "FFFFFF"))
            }
        }
    }
    
}
