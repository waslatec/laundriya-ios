//
//  OrderLaterV.swift
//  Washer-Client
//
//  Created by Amr on 2/6/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import UIKit
import DropDown
import CoreLocation
import CCMPopup
import DateTimePicker
class OrderLaterVC: UIViewController {
    
    @IBOutlet weak var userLocationAddress: UILabel!
    let drop = DropDown()
    var clientAddresses = [Address]()
    var postAddress : Address?
    var orderID = ""
    var selectedDate: Date?
    @IBOutlet weak var Notes: UITextField!
    @IBOutlet weak var DateLabel : UILabel!
    @IBOutlet weak var sendOrderButton: UIButton!
    
    var catids :[Int] = []
    var Categories = [CategoryDTO]()
    var picker: DateTimePicker!

    var map : MapVC?
    var userLocation : lookupAddress?
    var orderId = 0
    let formatterr = DateFormatter()
    let someDateTime = DateFormatter()
    var userNumber = ""
    var orderToBePost = [Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        self.hideKeyboardWhenTappedAround()
        
        formatterr.dateFormat = "yyyy/MM/dd"
        
        userNumber =  (UserDefaults.standard.object(forKey: "mobileNumber") as? String)!
        self.userLocationAddress.text = postAddress!.deliveryAddress
        let location = CLLocation(latitude: postAddress!.lat!, longitude: postAddress!.lng!)
        self.map?.getLocation(location: location)
        //        GetAdresses()
    }
    
    @IBAction func showDatePopUp()
    {
        picker = DateTimePicker.show(selected: Date().addDaysToCurrentDate(numofDays: 1).trimTime, minimumDate: Date().addDaysToCurrentDate(numofDays: 1).trimTime, maximumDate: Date().addDaysToCurrentDate(numofDays: 30))
        picker.doneButtonTitle = "Done"
        
        picker.AddBorderWithColor()
        picker.darkColor = UIColor(fromARGBHexString: "01B0F0")
        picker.highlightColor = UIColor(fromARGBHexString: "01B0F0")
        
                picker.completionHandler = { date in
                    // do something after tapping done
                    self.selectedDate = date
                    self.DateLabel.text = date.asStringWithTime
                    print(date.asStringWithTime)
                }

    }
//    func GetAdresses()
//    {
//        Networking.AddressesList { (addressesList) in
//            self.clientAddresses = []
//            self.clientAddresses.append(contentsOf: addressesList.address ?? [])
//            self.setupChooseDropDown()
//        }
//    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func setupChooseDropDown() {
//        drop.anchorView = userLocationAddress
//        drop.bottomOffset = CGPoint(x: 0, y: userLocationAddress.bounds.height)
//        drop.dataSource = self.clientAddresses.map({ (item) in
//            return item.deliveryAddress!
//        })
//        
//        // Action triggered on selection
//        drop.selectionAction = { [unowned self] (index, item) in
//            self.userLocationAddress.text = item
//            let address = self.clientAddresses[index]
//            self.postAddress = self.clientAddresses[index]
//            let location = CLLocation(latitude: address.lat!, longitude: address.lng!)
//            self.map?.getLocation(location: location)
//        }
//    }
    
    @IBAction func Cancel()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func ShowDropDown()
    {
//        self.drop.show()
    }
    @IBAction func postOrder(_ sender: AnyObject) {
        if selectedDate == nil
        {
            showDatePopUp()
            return
        }
        let someDateTime = formatterr.string(from: selectedDate!)
        if self.postAddress == nil  {
            _ = SweetAlert().showAlert(NSLocalizedString("خطا", comment: "LocalizedStringin OrderNowVC"), subTitle: NSLocalizedString("اختر عنوان", comment: "LocalizedStringin OrderNowVC"), style: .error)
            //              SwiftOverlays.removeAllBlockingOverlays()
            
        }else{
            
            let lat = postAddress!.lat
            let lng = postAddress!.lng
            let addressId = postAddress!.id//! as Int
            orderToBePost.removeAll()
            orderToBePost.append(catids)
            orderToBePost.append(lat!)
            orderToBePost.append(lng!)
            orderToBePost.append(addressId!)
            orderToBePost.append(0.0)
            orderToBePost.append(2)
            orderToBePost.append(true)
            orderToBePost.append(someDateTime)
            orderToBePost.append(false)
            orderToBePost.append(userNumber)
            performSegue(withIdentifier: "showConfirmRequest", sender: nil)
        }
        
    }
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "mapVC"{
            
            map = (segue.destination as! MapVC)
//            map!.userAddress = { (address) in
//                
//                if address.lng != nil && address.lat != nil
//                {
//                    MapTasks.geocodeAddresswithlatlng(lat: address.lat!, lng: address.lng!, completion: { (lookupad) in
//                        if (lookupad.resultsAdd?.count)! > 0
//                        {
//                            self.userLocation = lookupad.resultsAdd![0]
//                            //                            self.userLocationAddress.text = lookupad.resultsAdd![0].formateer
//                        }
//                    })
//                }
//                
//            }
            
            
        }
        else if segue.identifier == "showConfirmRequest"  {
            
            let vc = segue.destination as! PromotionVC
            vc.category = self.Categories
            
            vc.view.layer.cornerRadius = 15
            vc.successfullCompletionHandler = {
                vc.dismiss(animated: true, completion: nil)
                self.perform(#selector(self.PostTheOrder), with:self, afterDelay: 0.5)
            }
            let popupSegue: CCMPopupSegue? = segue as? CCMPopupSegue
            popupSegue?.destinationBounds = CGRect(x: 0, y: 0, width: 280, height: 300)
            popupSegue?.backgroundViewColor = UIColor(fromARGBHexString: "456DB1")
            popupSegue?.backgroundViewAlpha = 0.9
            popupSegue?.backgroundBlurRadius = 0.1
            popupSegue?.dismissableByTouchingBackground = false
            
        }
        else if segue.identifier == "ProcessOrder"  {
            
            let vc = segue.destination as! SearchDriversVC
            vc.orderData = self.orderToBePost
            vc.cc = self.Categories
            vc.isOrderNow = false
            vc.ViewWidth = 20
            vc.successfullCompletionHandler = {(orderId) in
                vc.dismiss(animated: true, completion: nil)
                self.orderID = orderId
                self.perform(#selector(self.OrderPostedSuccess), with:self, afterDelay: 0.5)
            }
            vc.failureCompletionHandler = {(orderId) in
                vc.dismiss(animated: true, completion: {
                    self.navigationController?.popToRootViewController(animated: true)
                })
                
                if Languages.currentAppleLanguage() == "ar" {
                    DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: {
                        
                        _ = SweetAlert().showAlert(NSLocalizedString("طلب رقم\(orderId)", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("لا يوجد سائقين", comment: "Localize me Label in the ConfirmVC scene"), style: .warning)
                        
                        
                    }        )
                }else {
                    DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: {
                        
                        _ = SweetAlert().showAlert(NSLocalizedString("Order No.\(orderId) ", comment: "Localize me Label in the ConfirmVC scene") , subTitle: NSLocalizedString("No driver", comment: "Localize me Label in the ConfirmVC scene"), style: .warning)
                        
                    }        )
                }
                
                
            }
            let popupSegue: CCMPopupSegue? = segue as? CCMPopupSegue
            popupSegue?.destinationBounds = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            popupSegue?.backgroundViewColor = UIColor(fromARGBHexString: "456DB1")
            popupSegue?.backgroundViewAlpha = 0.9
            popupSegue?.backgroundBlurRadius = 0.1
            popupSegue?.dismissableByTouchingBackground = false
            
        }
        else if segue.identifier == "OrderSuccess"  {
            
            let vc = segue.destination as! OrderSuccessVC
            vc.isOrderNow = false
            
            vc.successfullCompletionHandler = {
                vc.dismiss(animated: true, completion: {
                    self.navigationController?.popToRootViewController(animated: true)
                })
            }
            vc.failureCompletionHandler = {
                vc.dismiss(animated: true, completion: {
                    self.navigationController?.popToRootViewController(animated: true)
                })
                
                
                
            }
            let popupSegue: CCMPopupSegue? = segue as? CCMPopupSegue
            popupSegue?.destinationBounds = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            popupSegue?.backgroundViewColor = UIColor(fromARGBHexString: "456DB1")
            popupSegue?.backgroundViewAlpha = 0.9
            popupSegue?.backgroundBlurRadius = 0.1
            popupSegue?.dismissableByTouchingBackground = false
            
        }
        
    }
    
    func OrderPostedSuccess() {
        performSegue(withIdentifier: "OrderSuccess", sender: nil)
        
    }
    
    func PostTheOrder() {
        
        performSegue(withIdentifier: "ProcessOrder", sender: nil)
        
    }
    
}

