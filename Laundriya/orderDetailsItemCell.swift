//
//  orderDetailsItemCell.swift
//  Laundriya
//
//  Created by Yo7ia on 5/7/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//



import UIKit

class orderDetailsItemCell: UITableViewCell {
    
    @IBOutlet weak var item: UILabel!
    @IBOutlet weak var service: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var qty: UILabel!
    @IBOutlet weak var Unitprice: UILabel!
    @IBOutlet weak var Totalprice: UILabel!


    var order: Item?
    var delegate : ItemsCellDelagete?
    override func awakeFromNib() {
        super.awakeFromNib()
        //        orderID.layer.cornerRadius = 8
        //        orderidd.layer.cornerRadius = 8
        //        orderStatus.layer.cornerRadius = 8
    }
    
    func SetupCell()
    {
        item.text = Languages.currentAppleLanguage() == "ar" ? order!.arabicName! : order!.englishName!
        service.text = Languages.currentAppleLanguage() == "ar" ? order!.serviceArabicName! : order!.serviceEnglishName!
        type.text = Languages.currentAppleLanguage() == "ar" ? order!.serviceTypeArabicName! : order!.serviceTypeEnglishName!
        qty.text = "\(order!.amount!)"
        Unitprice.text = "\(order!.unitePrice!)"
        Totalprice.text = "\(order!.amount!*order!.unitePrice!)"
    }
    
    
}
