//
//  PricingItemCell.swift
//  Laundriya
//
//  Created by Yo7ia on 5/8/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//




import UIKit

class PricingItemCell: UITableViewCell {
    
    @IBOutlet weak var service: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var Item: UILabel!
    @IBOutlet weak var Totalprice: UILabel!
    
    
    var order: PriceListModel?
    var delegate : ItemsCellDelagete?
    override func awakeFromNib() {
        super.awakeFromNib()
        //        orderID.layer.cornerRadius = 8
        //        orderidd.layer.cornerRadius = 8
        //        orderStatus.layer.cornerRadius = 8
    }
    
    func SetupCell()
    {
        Item.text = Languages.currentAppleLanguage() == "ar" ? order!.ItemArabicName : order!.ItemEnglishName
        service.text = Languages.currentAppleLanguage() == "ar" ? order!.ServiceArabicName : order!.ServiceEnglishName
        type.text = Languages.currentAppleLanguage() == "ar" ? order!.ServiceTypeArabicName : order!.ServiceTypeEnglishName
        Totalprice.text = Languages.currentAppleLanguage() == "en" ? "\(order!.Price) SAR" : "\(order!.Price) ريال"    }
    
    
}
