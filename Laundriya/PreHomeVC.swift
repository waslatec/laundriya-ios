//
//  PreHomeVC.swift
//  Laundriya
//
//  Created by Yo7ia on 5/3/17.
//  Copyright © 2017 Yo7ia. All rights reserved.
//

import Foundation
import UIKit
class PreHomeVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        performSegue(withIdentifier: "goToHome", sender: nil)
    }
}
