//
//  MapTasks.swift
//  Washer-Client
//
//  Created by Mohamed Namir on 2/8/17.
//  Copyright © 2017 WaslTec. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire

class MapTasks: NSObject {
    
    static let baseURLGeocode = "https://maps.googleapis.com/maps/api/geocode/json?"
    
    static let APIKey = "AIzaSyBIIJaRLCaeC0e_wglmnrJfX5tmCeN_M1Y"
    
    var lookupAddressResults: Dictionary<NSObject, AnyObject>!
    
    var fetchedFormattedAddress: String!
    
    var fetchedAddressLongitude: Double!
    
    var fetchedAddressLatitude: Double!
    
    override init() {
        super.init()
    }
    
     class func geocodeAddress(address: String, completion : @escaping (lookupAddressResults) -> () ){
        let url = baseURLGeocode
       let headers = ["CA&key":"\(APIKey)"]
       let parm = ["address": "\(address)"]
        Alamofire.request(url, method: .get, parameters: parm, headers: headers).responseString { Result in
            if let Json = Result.result.value {
               // print(Json)
                completion(Mapper<lookupAddressResults>().map(JSONString: Json)!)
            }else{
                print("error")
            }
            
        }
    }
    class func geocodeAddresswithlatlng(lat: Double,lng: Double, completion : @escaping (lookupAddressResults) -> () ){
        let url = self.baseURLGeocode
        let headers = ["CA&key":"\(self.APIKey)"]
        let parm = ["latlng": "\(lat),\(lng)"]
        Alamofire.request(url, method: .get, parameters: parm, headers: headers).responseString { Result in
            if let Json = Result.result.value {
               // print(Json)
                completion(Mapper<lookupAddressResults>().map(JSONString: Json)!)
            }else{
                print("error")
            }
            
        }
    }
        
    }


class lookupAddressResults: Mappable {
    var resultsAdd : [lookupAddress]?
    
    init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        resultsAdd <- map["results"]
        
    }
    
}
class lookupAddress: Mappable {
    
    var lat : Double?
    var lng : Double?
    var formateer : String?
    
    init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
       lat <- map["geometry.location.lat"]
        lng <- map["geometry.location.lng"]
        formateer <- map["formatted_address"]
        
        
    }
}
